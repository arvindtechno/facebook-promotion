<?php session_start();
if(!isset($_SESSION['login_user'])) {
header("location: index.php");
exit();
}

date_default_timezone_set('asia/kolkata');
include ('fb_php_sdk/config.php');
include ('fb_php_sdk/facebook.php');
include('php/connection.php');
$conn = mysqli_connect($host, $username, $password, $database)or die("connection lost");
?>
<!doctype html>
<html>
    <head>
        <title>User Dashboard</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

    </head>
    <body>

        <!----------------------------- Header Part -------------------------------------->
        <div class="header" style="min-height: 50px">
            <div class="row">
                <div class="logo col-md-1">
                    
                </div>
                <div class="heading col-md-7">
                    <h1 style="margin-left: 25%;">Facebook Promotion Software</h1>
                </div>
                <div class="col-sm-1"  style="padding-top: 25px;">
                     <a class="btn btn-primary" href="userpanel.php?i=<?php echo $i = $_GET['i']; ?>" id="edit_user" name="edit_user">User Panel</a>
                </div> 
                <div class=" col-md-1" style="padding-top: 25px;">
                    <a href="logout.php" class="btn btn-primary" style="float: right">Logout</a>
                </div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
<?php 
$facebook = new Facebook(array(
    'appId' => APP_ID,
    'secret' => APP_SECRET,
    'fileUpload' => true,
        ));
Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;

$user = $facebook->getUser();
if ($user) {
    $fb_post_id = array();
    $d = array();
   $select = "SELECT * FROM `posts_details` WHERE `user_id` = '$user'";
   $result = mysqli_query($conn, $select);
   if($result){
       while ($row = mysqli_fetch_array($result)){
           $fb_post_id[] = $row['fb_post_id'];
       }
   }
   foreach($fb_post_id as $key){
        try {

        $user_profile = $facebook->api('/'.$key,'GET');

        } catch(FacebookApiException $e) {
          $d[] = $key;
      }   
   }
   foreach ($d as $key){
       $update = "UPDATE `posts_details` SET `deleted_post` = 1 WHERE `fb_post_id` = '$key'" ;
       mysqli_query($conn, $update);
   }
?>
        <div class="container">
            <div class="row">
              
                <div class="col-md-12"  style="padding-top:25px">
                    <?php 
                    $id = $_GET['i'];
                    $select = "SELECT * FROM `user_registration` WHERE `user_id` = $id";

                    $result = mysqli_query($conn, $select);
                    //if($result){
                    while ($row = mysqli_fetch_array($result)) {
                        $name = $row['user_name'];
                        $email = $row['user_email'];
                        $sdate = $row['start_date'];
                        $edate = $row['end_date'];
                        
                    }
                  //  }
                    ?>
                    <table class="table table-hover  table-bordered"  style="margin:1px">
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>E-MAIL</th>
                        </tr>
                        <tr>
                            <td><?php echo $id; ?></td>
                            <td><?php echo $name; ?></td>
                            <td><?php echo $email; ?></td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <?php
            $k = 1;
             $feed_place = array();
            $date_t = array();
            $post_type = array();
            $published_t = array();
            $post = array();
            $delete_post = array();
            $q="SELECT * FROM `posts_details` where `user_id` = '$user'";

            $r=  mysqli_query($conn, $q);
            if($r){
            $total=  mysqli_num_rows($r);
            while($row = mysqli_fetch_array($r)){
                $feed_place[] = $row['feed_place'];
                $date_t[] = $row['publish_date'];
                $post_type[] = $row['post_type'];
                $published_t[] = $row['published'];
                $post[] = $row['fb_post_id'];
                $delete_post[] = $row['deleted_post'];
                 $pname[]=$row['place_name'];
            }
            }
        ?>
            <div class="row">
                <h3>Total Post</h3>
                <table class="table table-hover  table-bordered"  style="margin:1px">
                    <thead>
                        <th>SR.NO</th>
                        <th>Feed Place</th>
                        <th>Feed Place Name</th>
                        <th>Date</th>
                        <th>Post Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        <?php
                        for($i=0;$i<$total;$i++):
                        ?>
                        <tr class="remove_<?php echo $post[$i]; ?>">
                            <td><?php echo $i+1 ?></td>
                            <td><?php echo $feed_place[$i] ?></td>
                             <td><?php echo $pname[$i] ?></td>
                            <td><?php echo $date_t[$i] ?></td>
                            <td><?php echo $post_type[$i] ?></td>
                            <td><?php if($post[$i]==''){
                                    echo 'Blocked';
                                }  else if($post[$i] == 1){
                                    echo 'Deleted';
                                }else{
                                    echo 'Published';
                                }
                                 ?></td>
                            <td><input type="button" class="btn btn-danger" id="<?php echo $post[$i]; ?>" value="Remove"/></td>
                                
                        </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
            
            <div class="row">
                <h3>Blocked Post</h3>
                <table class="table table-hover  table-bordered"  style="margin:1px">
                    <thead>
                    <th>SR.NO</th>
                        <th>Feed Place</th>
                          <th>Feed Place Name</th>
                        <th>Date</th>
                        <th>Post Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        <?php $k = 0;
                        for ($i=0;$i<$total;$i++):
                            if($post[$i]==''): ?>
                        <tr class="remove_<?php echo $post[$i]; ?>">
                           <td><?php echo $k = $k+1 ?></td>
                            <td><?php echo $feed_place[$i] ?></td>
                            <td><?php echo $pname[$i] ?></td>
                            <td><?php echo $date_t[$i] ?></td>
                            <td><?php echo $post_type[$i] ?></td>
                            <td>Blocked</td>
                            <td><input type="button" class="btn btn-danger" id="<?php echo $post[$i]; ?>" value="Remove"/></td>
                        </tr>
                        <?php endif;
                        endfor; ?>
                    </tbody>
                </table>
            </div>
            
             <div class="row">
                <h3>Deleted Post</h3>
                <table class="table table-hover  table-bordered"  style="margin:1px">
                    <thead>
                    <th>SR.NO</th>
                        <th>Feed Place</th>
                          <th>Feed Place Name</th>
                        <th>Date</th>
                        <th>Post Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        <?php $k = 0;
                        for ($i=0;$i<$total;$i++):
                            if($delete_post[$i] == 1): ?>
                        <tr class="remove_<?php echo $post[$i]; ?>">
                           <td><?php echo $k = $k+1 ?></td>
                            <td><?php echo $feed_place[$i] ?></td>
                            <td><?php echo $pname[$i] ?></td>
                            <td><?php echo $date_t[$i] ?></td>
                            <td><?php echo $post_type[$i] ?></td>
                            <td>Deleted</td>
                            <td><input type="button" class="btn btn-danger" id="<?php echo $post[$i]; ?>" value="Remove"/></td>
                        </tr>
                        <?php endif;
                        endfor; ?>
                    </tbody>
                </table>
            </div>
            
        </div>
<?php
 }else{
     ?>
        <div class="container">
            <pre>Please Login First</pre>

        </div>
        <?php
}

?>
        <script>
            $("tr>td>.btn").click(function(e){
                var id = $(this).attr("id");
                var dataString = 'user_post='+id;
                if(id.length>0){
        $.ajax({
        type: "POST",
        url: "ajaxcall.php",
        data: dataString,
        cache: false,
        success: function(data){
        if(data == 'true'){
            $(".remove_"+id).remove();
        e.preventDefault();
        }
        else{
            alert(false);
            e.preventDefault();
        }
        }
       });
                }
                else{
                    e.preventDefault();
                }
                });
            </script>
    </body>
</html>
