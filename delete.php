<!doctype html>
<?php ob_start(); ?>
<?php
include('php/connection.php');
$conn = mysqli_connect($host, $username, $password, $database)or die("connection lost"); ?>
<html>
    <head>
        <title>Admin Page Poster</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </head>
    <body>
        <!----------------------------- Header Part -------------------------------------->
        <div class="header" style="min-height: 50px">
            <div class="row">
                <div class="logo col-md-2">
                    
                </div>
                <div class="heading col-md-8">
                    <h1 style="margin-left: 30%;">Facebook Promotion Software</h1>
                </div>
                <div class=" col-md-1" style="padding-top: 25px;">
                    <a href="logout.php" class="btn btn-primary" style="float: right">Logout</a>
                </div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
        <div class="container">
            <div class="row">
                <h3>Welcome Admin</h3>
                <div class="col-md-3 add-user">
                    <a href="adduser.php" type="button" class="btn btn-primary">ADD USER</a>
                </div>
                <div class=" col-md-3 manage-user">
                    <a href="manageuser.php" type="button" class="btn btn-primary">MANAGE USER</a>
                </div>
                <div class=" col-md-3 un-renew-user">
                    <a href="renewuser.php" type="button" class="btn btn-primary">UN RENEW USER</a>
                </div>
<!--                <div class=" col-md-3 user-dashboard">
                    <a href="dashboard.php" type="button" class="btn btn-primary">DASHBOARD</a>
                </div>-->
                <div class="col-md-12"  style="padding-top:25px">
                    
                    <form class="form-horizontal" role="form" action="php/config.php" method="post">
                     <?php
                     $id = $_GET['i'];
                    //$conn = mysqli_connect("localhost", "root", "", "facebook_promotion");
                    $select = "SELECT * FROM `user_registration` where `user_id`= '".$id."'";
                    //var_dump($select);
                    $result=  mysqli_query($conn, $select);
                    while ($row = mysqli_fetch_array($result)){
                        $user_name = $row['user_name'];
                        $user_email = $row['user_email'];
                        $sdate = $row['start_date'];
                        $edate = $row['end_date'];
                    }
                    ?>
                    <table class="table table-hover  table-bordered"  style="margin:1px">
                        <tr>
                            <th>USER ID</th>
                            <th>USER NAME</th>
                            <th>E-MAIL</th>
                            
                        </tr>
                        <tr>
                            <td><?php echo $id; ?></td>
                            <td><?php echo $user_name; ?></td>
                            <td><?php echo $user_email; ?></td>
                        </tr>
                    </table>
                        <input type="hidden" name="id" value="<?php echo $id ?>"/>
                    <button class="btn btn-success" id="registration" type="submit" name="delete_user" >DELETE USER</button>
                    <a href="manageuser.php" class="btn btn-danger">CANCEL</a>
                    </form>
                    
                </div>
            </div>
        </div>
        
        
        <!----------------------------- footer Part -------------------------------------->
        
        
    </body>
</html>
  <? ob_flush(); ?>      
