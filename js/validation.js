/**
 * login form validation
 */

$(document).ready(function(){
    
   $('#login').click(function(e){
      var email = $('#email').val(); 
        var password = $('input#password').val();
    
     if( email == 0){
            $('span.email').addClass('glyphicon-remove');
            $('span.lemail').html('Please Enter Your Email Address');
            e.preventDefault();
        } else{
            $('span.email').addClass('glyphicon-ok');
        }

   
    if(password == 0){
         $('span.pass').addClass('glyphicon-remove');
         $('span.rpass').html('password is required');
         e.preventDefault();
     }
     
   });
/**
*user login form validation
*/
$('#user_login').click(validateuserlogin);
$('#login').click(validateadminlogin);

//    $("#user_email").change(validateusername);
//    $('#user_password').keyup(validateusername);
//    e.preventDefault();
//});

});

/**
 * user registration form validation
 */

  
$(document).ready(function(){
    
    $("#uemail").change(validateusername);
    $('#uemail').keyup(validateusername);
   
    $('input.fbpro').change(validatefbprofile);
    $('input.fbpro').keyup(validatefbprofile);
    
    $('button#add_more').click(function(e){
           //alert('hello');
           var row = $('<div class="form-group has-success has-feedback fb-profile"><label class="control-label col-sm-2" for="add_profile">ADD FB PROFILE</label><div class="col-sm-4"><input type="hidden" name="pro-id[]" value="NULL"/><input type="text" class="form-control fbpro" id="fbprofile" name="add_profile[]" aria-describedby="inputSuccess3Status"><span class="glyphicon fbprofile form-control-feedback" aria-hidden="true"></span><span id="inputSuccess3Status" class="sr-only">(success)</span><span class="profilereq"></span></div></div>');
           if($("div.fb-profile").length <6){
               $("div#row1").append(row);
               $("input#remove_field").attr({type:"button"});
           }
           e.preventDefault();
       }) ;
    $('input#remove_field').click(function(e){
        //alert('hello');
        $("div#row1:last>div.fb-profile").last('.fb-profile').remove();
        e.preventDefault();
    });   
    $('button#registration').click(function(e){
    var uname = $('#uname').val();
     if( uname == 0){
            $('span.name').addClass('glyphicon-remove');
            $('span.namereq').html('User Name Required');
            e.preventDefault();
        } else{
            $('span.name').addClass('glyphicon-ok');
        }
    var uemail = $('#uemail').val();
     if( uemail == 0){
            $('span.email').addClass('glyphicon-remove');
            $('span.emailreq').html('User Email Required');
            e.preventDefault();
        } else{
            check_availability();
            //$('span.email').addClass('glyphicon-ok');
        }
    var profile = $('#add_profile').val();
     if( profile == 0){
            $('span.fbprofile').addClass('glyphicon-remove');
            $('span.profilereq').html('User Profile Required');
            e.preventDefault();
        } else{
            $('span.fbprofile').addClass('glyphicon-ok');
        }
     var sdate = $('#start_date').val();
     if( sdate == 0){
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Start Date Required');
            e.preventDefault();
        } else{
            if(isDate(sdate)){
            $('span.sdate').addClass('glyphicon-ok');
        }else{
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Invalid Date Format Please Enter Date in YYYY-MM-DD');
            e.preventDefault();
        }
        }
     var edate = $('#end_date').val();
     if( edate == 0){
         if(isDate(edate)){
            $('span.sdate').addClass('glyphicon-ok');
        }else{
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Invalid Date Format Please Enter Date in YYYY-MM-DD');
            e.preventDefault();
        }
            $('span.edate').addClass('glyphicon-remove');
            $('span.edatereq').html('End Date Required');
            e.preventDefault();
        } else{
            $('span.edate').addClass('glyphicon-ok');
        }   
}) ;
$('button#add-registration').click(function(e){
    var uname = $('#uname').val();
     if( uname == 0){
            $('span.name').addClass('glyphicon-remove');
            $('span.namereq').html('User Name Required');
            e.preventDefault();
        } else{
            $('span.name').addClass('glyphicon-ok');
        }
    var uemail = $('#uemail').val();
     if( uemail == 0){
            $('span.email').addClass('glyphicon-remove');
            $('span.emailreq').html('User Email Required');
            e.preventDefault();
        } else{
            check_availability();
            //$('span.email').addClass('glyphicon-ok');
        }
    var profile = $('#add_profile').val();
     if( profile == 0){
            $('span.fbprofile').addClass('glyphicon-remove');
            $('span.profilereq').html('User Profile Required');
            e.preventDefault();
        } else{
            $('span.fbprofile').addClass('glyphicon-ok');
        }
     var sdate = $('#s_date').val();
     if( sdate == 0){
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Start Date Required');
            e.preventDefault();
        } else{
            if(isDate(sdate)){
            $('span.sdate').addClass('glyphicon-ok');
        }else{
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Invalid Date Format Please Enter Date in YYYY-MM-DD');
            e.preventDefault();
        }
        }
     var edate = $('#e_date').val();
     if( edate == 0){
         if(isDate(edate)){
            $('span.sdate').addClass('glyphicon-ok');
        }else{
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Invalid Date Format Please Enter Date in YYYY-MM-DD');
            e.preventDefault();
        }
            $('span.edate').addClass('glyphicon-remove');
            $('span.edatereq').html('End Date Required');
            e.preventDefault();
        } else{
            $('span.edate').addClass('glyphicon-ok');
        }   
}) ;

   var sdate = {
            altField : "start date",
            changeMonth : true,
            changeYear : true  ,
            showAnim : "drop",
            duration : "slow",
            dateFormat : "yy-mm-dd"
        };
        $('input#start_date').datepicker(sdate);
         $('input#start_d').datepicker(sdate);
          $('input#s_date').datepicker(sdate);

    var edate = {
            altField : "start date",
            changeMonth : true,
            changeYear : true  ,
            showAnim : "drop",
            duration : "slow",
            dateFormat : "yy-mm-dd"
        };
        $('input#end_date').datepicker(edate);
        $('input#end_d').datepicker(edate);
        $('input#e_date').datepicker(edate);
        
        $("tr>td>.btn").click(function(){
                var name = $(this).attr("name");
                //var val = name.split("_");
                var id = name.split("_");
                //alert(id[1]);
                if(id[0] == 'delete'){
                    //alert(val);
                    window.location.href ='delete.php?i='+id[1];
                }else if(id[0] == 'edit'){
                    //alert(id);
                window.location.href ='edit.php?i='+id[1];
            }else if(id[0] == 'dashboard'){
                 window.location.href ='dashboard.php?i='+id[1];
            }
	});
        $("input#days").click(function(e){
           var day = $(this).attr("name");
           var id = day.split(" ");
           if(id[1]<0){
               window.open('renewpopup.php?i='+id[0], 'newwindow', 'width=600, height=700');
           }else{
               e.preventDefault();
           }
        });

});
function isDate(txtDate)
{
    var currVal = txtDate;
    if(currVal == '')
        return false;

    var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null) 
        return false;

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[3];
    dtDay= dtArray[5];
    dtYear = dtArray[1];        

    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                return false;
    }
    return true;
}


function validateusername(e){
       $('.emailreq').load('ajaxcall.php',{'uemail':$(this).val()})
    }
function validatefbprofile(e){
       $('.profilereq').load('ajaxcall.php',{'fbprofile':$(this).val()})
    }
    
function validateuserlogin(e){
    
        var user_email=$("#user_email").val();
        var password=$("#user_password").val();
        var dataString = 'user_email='+user_email+'&user_password='+password;
        if($.trim(user_email).length>0 && $.trim(password).length>0){
        $.ajax({
        type: "POST",
        url: "ajaxcall.php",
        data: dataString,
        cache: false,
        success: function(data){
        if(data == 'true'){
        }
        else{
            $("span.lemail").html("Error: Invalid username and password. ");
            e.preventDefault();
            return false;
        }
        }
        });
        
        }else{
            e.preventDefault();
        }
}    
function validateadminlogin(e){
    var email = $('#email').val(); 
        var password = $('input#password').val();
    var dataString = 'admin_email='+email+'&admin_password='+password;
        if($.trim(email).length>0 && $.trim(password).length>0){
        $.ajax({
        type: "POST",
        url: "ajaxcall.php",
        data: dataString,
        cache: false,
        success: function(data){
        if(data == 'true'){
            
        }
        else{
            $("span.rpass").html("Error: Invalid username and password. ");
            e.preventDefault();
              return false;
        }
        }
        });
      }  else{
            e.preventDefault();
        } 
}
/**
 * validation for renew pop window
 */
       $(document).ready(function(){
                $('#regis').click(function(e){
                     var sdate = $('#start_d').val();
     if( sdate == 0){
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Start Date Required');
            e.preventDefault();
        } else{
            if(isDate(sdate)){
            $('span.sdate').addClass('glyphicon-ok');
            
        }else{
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Invalid Date Format Please Enter Date in YYYY-MM-DD');
            e.preventDefault();
        }
        }
     var edate = $('#end_d').val();
     if( edate == 0){
         $('span.edate').addClass('glyphicon-remove');
            $('span.edatereq').html('End Date Required');
            e.preventDefault();
        } else{
            if(isDate(edate)){
            $('span.sdate').addClass('glyphicon-ok');
          //  window.close();
        }else{
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Invalid Date Format Please Enter Date in YYYY-MM-DD');
            e.preventDefault();
        }
        }   
                });
                
            $("button#registration1").click(function(e){
                
                var year = $("#year").val();
                var month = $("#month").val();
                var day = $("#day").val();
            if(year=='null' && month=='null' && day=='null'){
                $('span.date').html('min one field is required');
                e.preventDefault();
            }else{   
            //window.close();
            
        }
            });
            });