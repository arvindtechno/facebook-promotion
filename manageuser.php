<?php session_start();
if(!isset($_SESSION['login_admin'])) {
header("location: adminpanel.php");
exit();
}
include('php/connection.php');
$conn = mysqli_connect($host, $username, $password, $database)or die("connection lost");
?>
<!doctype html>
<html>
    <head>
        <title>Admin Page Poster</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </head>
    <body>
        <!----------------------------- Header Part -------------------------------------->
        <div class="header" style="min-height: 50px">
            <div class="row">
                <div class="logo col-md-1">
                    
                </div>
                <div class="heading col-md-6">
                    <h1 style="margin-left: 30%;">Facebook Promotion Software</h1>
                </div>
                <div class=" col-md-1" style="padding-top: 25px;">
                    <a href="logout.php" class="btn btn-primary" style="float: right">Logout</a>
                </div>
                <div class="col-md-4" style="padding-top:25px">
                    <form class="form-inline" role="form" action="search.php" method="post">
                            
                            <div class="form-group has-success has-feedback">
                                <input type="text" name="search" class="form-control" id="search-user" placeholder="Search By Name">
                                <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                            </div>
                        <button type="submit" name="search_user" class="btn btn-info">SEARCH</button>
                             
                    </form>
                </div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
        <div class="container">
            <div class="row">
                <h3>Welcome Admin</h3>
                <div class="col-md-3 add-user">
                    <a href="adduser.php" type="button" class="btn btn-primary">ADD USER</a>
                </div>
                <div class=" col-md-3 manage-user">
                    <a href="manageuser.php" type="button" class="btn btn-primary">MANAGE USER</a>
                </div>
                <div class=" col-md-3 un-renew-user">
                    <a href="renewuser.php" type="button" class="btn btn-primary">UN RENEW USER</a>
                </div>
                <div class=" col-md-3 un-renew-user">
                    <a href="settings.php" type="button" class="btn btn-primary">Settings</a>
                </div>
                <div class="col-md-12" style="padding-top:25px">
                    <h3 style="margin:1px">Manage User</h3>
                    <?php
                    //$conn = mysqli_connect("localhost", "root", "", "facebook_promotion");
                    $select = "SELECT * FROM `user_registration`";
                    $result=  mysqli_query($conn, $select);
                    $user_name = array();
                    $user_email = array();
                    $sdate = array();
                    $edate = array();
                    $id = array();
                    while ($row = mysqli_fetch_array($result)){
                        $user_name[] = $row['user_name'];
                        $user_email[] = $row['user_email'];
                        $sdate[] = $row['start_date'];
                        $edate[] = $row['end_date'];
                        $id[] = $row['user_id'];
                    }
                    ?>
                    <label class="control-label col-sm-2" for="checkbok" style="float:right;padding: 0px">Check To See Dashboard</label>
                    <table class="table table-hover table-bordered" style="margin:1px">
                        <thead>
                        <tr>
                            <th>SR.NO</th>
                            <th>USER NAME</th>
                            <th>E-MAIL</th>
                            <th>DURATION</th>
                            <th>Edit</th>
                            <th>Delete</th>
                            <th>RENEW</th>
                            <th>Dashboard<!--<input type="checkbox" id="selectall" style="margin-left:10px">--></th>
                        </tr>
                        </thead>
                        <?php
                        $c = count($user_name);
                        for($i=0;$i<$c;$i++):
                            $date1 = date_create();
                            $date2 = date_create($edate[$i]);
                            $diff=date_diff($date1,$date2);
                            
                        ?>
                        <tbody>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                            <td><?php echo $user_name[$i]; ?></td>
                            <td><?php echo $user_email[$i]; ?></td>
                            <td><?php $day = $diff->format("%R%a");
                            if($day<0){
                                echo '0 days';
                            }else{
                            echo $diff->format("%R%a days"); } ?></td>
                            <td><button class="btn btn-success" id="<?php echo $id[$i]; ?>" name="edit_<?php echo $id[$i]; ?>">edit</button></td>
                            <td><button class="btn btn-danger" id="<?php echo $id[$i]; ?>" name="delete_<?php echo $id[$i]; ?>">delete</button></td>
                            <td><input type="button" class="btn <?php $day = $diff->format("%R%a");
                                                            if($day<0){
                                                                echo 'btn-primary';
                                                            }else{
                                                            echo 'btn-info'; } ?> " id="days" name="<?php echo $id[$i]." ".$diff->format("%R%a"); ?>" value="renew"/></td>
                            <td><input type="button" class="btn btn-primary" name="dashboard_<?php echo $id[$i]; ?>" value="Dashboard" style="margin-left:20px"></td>
                        </tr>
                        </tbody>
                        <?php endfor; ?>
                    </table>
                    
                    
                </div>
            </div>
        </div>
        
        
        <!----------------------------- footer Part -------------------------------------->
        
        
    </body>
</html>