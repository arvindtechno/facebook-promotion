<?php session_start();
if(!isset($_SESSION['login_admin'])) {
header("location: adminpanel.php");
exit();
}
include('php/connection.php');
$conn = mysqli_connect($host, $username, $password, $database)or die("connection lost"); ?>
<!doctype html>
<html>
    <head>
        <title>Admin Page Poster</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </head>
    <body>
        <!----------------------------- Header Part -------------------------------------->
         <div class="header" style="min-height: 50px">
            <div class="row">
                <div class="logo col-md-1">
                    
                </div>
                <div class="heading col-md-6">
                    <h1 style="margin-left: 30%;">Facebook Promotion Software</h1>
                </div>
                <div class=" col-md-1" style="padding-top: 25px;">
                    <a href="logout.php" class="btn btn-primary" style="float: right">Logout</a>
                </div>
                 <div class="col-md-4" style="padding-top:25px">
                    <form class="form-inline" role="form" action="search.php" method="post">
                            
                            <div class="form-group has-success has-feedback">
                                <input type="text" name="search" class="form-control" id="search-user" placeholder="Search By Name">
                                <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                            </div>
                        <button type="submit" name="search_user" class="btn btn-info">SEARCH</button>
                             
                    </form>
                </div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
        <div class="container">
            <div class="row">
                <h3>Welcome Admin</h3>
                <div class="col-md-3 add-user">
                    <a href="adduser.php" type="button" class="btn btn-primary">ADD USER</a>
                </div>
                <div class=" col-md-3 manage-user">
                    <a href="manageuser.php" type="button" class="btn btn-primary">MANAGE USER</a>
                </div>
                <div class=" col-md-3 un-renew-user">
                    <a href="renewuser.php" type="button" class="btn btn-primary">UN RENEW USER</a>
                </div>
                     <?php
                    $select = "SELECT `user_id`,`user_email`  FROM user_registration";
                    $email = array();
                    $id = 0;
                    $result = mysqli_query($conn, $select);
                   
                    while($row = mysqli_fetch_array($result)){
                    $id = $row['user_id'];    
                    $email = $row['user_email'];
                    }
                    
                    
                    ?>
                
                <div class="row">
                    <div  class="col-md-12 adduser">
                        <h3 class="control-label" for="uid" style="padding-bottom: 10px">Add New User Pannel</h3>
                        <form class="form-horizontal" role="form" action="php/config.php" method="post">
                            <div class="form-group has-success has-feedback fb-profile">
                              <label class="control-label col-sm-2" for="uid">USER ID</label>
                              <div class="col-sm-4">
                                  <input type="text" class="form-control" id="uid" name="uid" readonly value="<?php  echo $id+1; ?>" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                              </div>
                            </div>
                            <div class="form-group has-success has-feedback fb-profile">
                              <label class="control-label col-sm-2" for="uname">USER NAME</label>
                              <div class="col-sm-4">
                                  <input type="text" class="form-control" id="uname" name="uname" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon name form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="namereq"></span>
                              </div>
                            </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="uemail">USER E-MAIl</label>
                              <div class="col-sm-4">
                                  <input type="email" class="form-control" id="uemail" name="uemail" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon email form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="emailreq"></span>
                              </div>
                            </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="add_profile">ADD FB PROFILE</label>
                              <div class="col-sm-4">
                               <input type="text" class="form-control fbpro" id="add_profile" name="add_profile[]" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon fbprofile form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="profilereq"></span>
                              </div>
                            </div>
                                <div id="row1">
                                    
                                </div>
                            <button class="btn btn-info" id="add_more" >ADD MORE PROFILE</button>
                            <input class="btn btn-danger" id="remove_field" type="hidden" value="REMOVE FIELD"/>
                             <div class="form-group has-success has-feedback">
                                <h4>Duration</h4>
                              <label class="control-label col-sm-2" for="start_date">START DATE</label>
                              <div class="col-sm-4">
                                  <input type="date" class="form-control" id="s_date" name="sdate" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon sdate form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="sdatereq"></span>
                              </div>
                             </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="end_date">END DATE</label>
                              <div class="col-sm-4">
                                  <input type="date" class="form-control" id="e_date" name="edate" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon edate form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="edatereq"></span>
                              </div>
                            </div>

                            <button class="btn btn-primary" id="add-registration" type="submit" name="user_registration" >SUBMIT DETAILS</button>
                    </form>
                </div>
             </div>
            </div>
        </div>
        
        
        <!----------------------------- footer Part -------------------------------------->
      
        
    </body>
</html>
        
<? ob_flush(); ?>
