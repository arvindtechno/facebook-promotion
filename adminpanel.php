<!doctype html>
<html>
    <head>
        <title>login Page Poster</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </head>
    <body>
          <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=601136476681840&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>     
        <!----------------------------- Header Part -------------------------------------->
        <div class="header container" style="min-height: 50px;padding-bottom:50px;">
             <div class="row">                
                  <div class="col-md-10" style="padding-top: 25px;padding-bottom:10px;">
                   <a href="index.php" class="btn btn-primary" style="float: right">Login as User</a>
                </div>
             </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-9">
                    <div class="row brdr headrow">
                <div class="logo col-md-4 nopadding">
                    <img src="img/photo.jpg" height="120" width="200" alt="fb_promo_logo"/>
                </div>
                <div class="heading col-md-8">
                    <h1>Facebook Promotion Software</h1>
                </div>
                </div></div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
        <div class="container">
            <div class="row ">
                 <div class="col-md-2"></div>
                <div class="brdr col-md-9">
                    <div class="row">
                <div  class="col-md-5 brdrp">
                  <div class="fb-like-box brdrr" data-href="https://www.facebook.com/FacebookDevelopers" data-width="300" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
                </div>
                 <div class="col-md-7">
                    
            <h3 class="newh3">Admin Login</h3>
            <form class="form-horizontal form_login" role="form" method="post" action="php/config.php">
                   
                    <div class="form-group has-success has-feedback">
                      <label class="control-label col-sm-3" for="Email">E-MAIl</label>
                      <div class="col-sm-9">
                          <input type="email" class="form-control" id="user_email" name="user_email" aria-describedby="inputSuccess3Status" placeholder="E-mail Address">
                        <span class="glyphicon email form-control-feedback" aria-hidden="true"></span>
                        <span id="inputSuccess3Status" class="sr-only">(success)</span>
                        <span class="lemail"></span>
                      </div>
                    </div>

                    <div class="form-group has-success has-feedback">
                      <label class="control-label col-sm-3" for="user_password">PASSWORD</label>
                      <div class="col-sm-9">
                          <input type="password" class="form-control" id="user_password" name="user_password" aria-describedby="inputSuccess3Status">
                        <span class="glyphicon pass form-control-feedback" aria-hidden="true"></span>
                        <span id="inputSuccess3Status" class="sr-only">(success)</span>
                        <span class="rpass"></span>
                      </div>
                    </div>
                <button class="btn btn-primary" id="user_login" type="submit" name="user_login" style="margin-left: 200px;">LOGIN NOW</button>
            </form>
                 </div> </div></div>
                 <div class="col-md-1"></div>
            </div>
        </div>
        <!----------------------------Footer Part --------------------------------------->
        <div class="footer">
            
        </div>

    </body>
</html>
<? ob_flush(); ?>
