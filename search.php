<?php
 include('php/connection.php');
 $conn = mysqli_connect($host, $username, $password, $database)or die("connection lost"); ?>
<!doctype html>
<html>
    <head>
        <title>Admin Page Poster</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </head>
    <body>
        <!----------------------------- Header Part -------------------------------------->
         <div class="header" style="min-height: 50px">
            <div class="row">
                <div class="logo col-md-1">
                    
                </div>
                <div class="heading col-md-8">
                    <h1 style="margin-left: 30%;">Facebook Promotion Software</h1>
                </div>
                <div class=" col-md-1" style="padding-top: 25px;">
                    <a href="logout.php" class="btn btn-primary" style="float: right">Logout</a>
                </div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
        <div class="container">
            <div class="row">
                <h3>Welcome Admin</h3>
                <div class="col-md-3 add-user">
                    <a href="adduser.php" type="button" class="btn btn-primary">ADD USER</a>
                </div>
                <div class=" col-md-3 manage-user">
                    <a href="manageuser.php" type="button" class="btn btn-primary">MANAGE USER</a>
                </div>
                <div class=" col-md-3 un-renew-user">
                    <a href="renewuser.php" type="button" class="btn btn-primary">UN RENEW USER</a>
                </div>
<!--                <div class=" col-md-3 user-dashboard">
                    <a href="dashboard.php" type="button" class="btn btn-primary">DASHBOARD</a>
                </div>-->
                <div class="col-md-12" style="padding-top:25px">
                    <h3 style="margin-left: 35%">SEARCH RESULT</h3>
                    <?php 
                      //$conn = mysqli_connect("localhost", "root", "", "facebook_promotion");
                        if(isset($_POST['search_user'])){
                            $user_name = $_POST['search'];
                            $select = "SELECT * FROM `user_registration` WHERE `user_name`= '".$user_name."'";
                            //var_dump($select);
                            $result = mysqli_query($conn, $select);
                            //var_dump($result);
                            if($result!=NULL):
                                while ($row = mysqli_fetch_array($result)):
                                   $date1 = date_create();
                                    $date2 = date_create($row['end_date']);
                                    $diff=date_diff($date1,$date2);
                                
                                ?>
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th>USER ID</th>
                                            <th>USER NAME</th>
                                            <th>E-MAIL</th>
                                            <th>DURATION</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <td><?php echo $row['user_id'] ?></td>
                                        <td><?php echo $row['user_name'] ?></td>
                                        <td><?php echo $row['user_email'] ?></td>
                                        <td><?php $day = $diff->format("%R%a");
                                                    if($day<0){
                                                        echo '0 days';
                                                    }else{
                                                    echo $diff->format("%R%a days"); } ?></td>
                                        <td><button class="btn btn-success" id="<?php echo $row['user_id']; ?>" name="edit_<?php echo $row['user_id']; ?>">edit</button></td>
                                        <td><button class="btn btn-danger" id="<?php echo $row['user_id']; ?>" name="delete_<?php echo $row['user_id']; ?>">delete</button></td>
                                        </tbody>
                      <?php endwhile;
                            endif;
                             if($result==NULL):   ?>
                                        <h3>SORRY RESULT NOT FOUND</h3>
                                        <a href="manageuser.php">....Back To The Page</a>
                                
                          <?php endif;
                        } ?>
                </div>
            </div>
        </div>
        
        
        <!----------------------------- footer Part -------------------------------------->
        
        
    </body>
</html>