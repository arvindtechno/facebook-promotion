<?php session_start();
if(!isset($_SESSION['login_user'])) {
header("location: index.php");
exit();
}
include('php/connection.php');
$conn = mysqli_connect($host, $username, $password, $database)or die("connection lost");
?>
<html>
    <head>
        <title>Admin Page Poster</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <!----------------------------- Header Part -------------------------------------->
         <div class="header" style="min-height: 50px">
            <div class="row">
                <div class="logo col-md-1">
                    
                </div>
                <div class="heading col-md-6">
                    <h1 style="margin-left: 30%;">Facebook Promotion Software</h1>
                </div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
        <div class="container">
            <div class="row">
                <h3>Welcome User</h3>
                <div class="col-md-3 ">
                    <a href="userpanel.php?i=<?php echo $i = $_GET['i']; ?>" class="btn btn-primary">Back To Panel</a>
                </div>
                
                <div  class="col-md-12 adduser">
                    <h3 class="control-label" for="uid" style="padding-bottom: 10px">Edit User Pannel</h3>
                        
                    <?php
                    //include ('config.php');
                    //$conn = mysqli_connect("localhost", "root", "", "facebook_promotion");
                    $id = $_GET['i'];
                    $select = "SELECT * FROM `user_registration` WHERE `user_id` = '".$id."'";
                    $result = mysqli_query($conn, $select);
                    while ($row = mysqli_fetch_array($result)) {
                        $name = $row['user_name'];
                        $email = $row['user_email'];
                        $password = $row['pass'];
                        
                    }
                    $profile = array();
                    $select = "SELECT * FROM `user_fb_profile` WHERE `user_id` = '".$id."'";
                    $result = mysqli_query($conn, $select);
                    while ($row = mysqli_fetch_array($result)) {
                        $profile[$row['profile_id']] = $row['profile_link'];
                    }
                    ?>
                    
                    
                    <form class="form-horizontal" role="form" action="php/config.php" method="post">
                        
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="uname">USER NAME</label>
                              <div class="col-sm-4">
                                  <input type="text" class="form-control" id="uname" name="uname" readonly value="<?php echo $name ?>" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon name form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="namereq"></span>
                              </div>
                            </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="uemail">USER E-MAIl</label>
                              <div class="col-sm-4">
                                  <input type="email" class="form-control" id="uemail" name="uemail" readonly value="<?php echo $email ?>" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon email form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="emailreq"></span>
                              </div>
                            </div>
                        
                         <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="password">Your Password :</label>
                              <div class="col-sm-4">
                                  <input type="password" class="form-control" id="pwd" name="pwd" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon sdate form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="pwd"></span>
                              </div>
                             </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="newpassword">Your New Password :</label>
                              <div class="col-sm-4">
                                  <input type="password" class="form-control" id="npwd" name="npwd" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon  npwd form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="pass"></span>
                              </div>
                            </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="newpassword">Confirm Password :</label>
                              <div class="col-sm-4">
                                  <input type="password" class="form-control" id="cpwd" name="cpwd" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon cpwd form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="npass"></span>
                              </div>
                            </div>
                        
                            <input type="hidden" name="id" value="<?php echo $id ?>"/>
                            <button class="btn btn-success" id="update_user" type="submit" name="update_user" >UPDATE DETAILS</button>
                            <a href="userpanel.php?i=<?php echo $i = $_GET['i']; ?>" class="btn btn-danger">CANCEL</a>
                    </form>
                    
                    
                </div>
            </div>
        </div>
        
        
        <!----------------------------- footer Part -------------------------------------->
        <script>
        /**
 * user update validation
 */
    $(document).ready(function(){
        $('#update_user').click(function(e){
            var pwd = $('#npwd').val();
            if(pwd == '' && pwd.length<5){
                $('span.npwd').addClass('glyphicon-remove');
                $('span.pass').html('password must contain 4 digits');
                e.preventDefault();
            }
            var cpwd = $('#cpwd').val();
            if (pwd != cpwd){
                $('span.cpwd').addClass('glyphicon-remove');
                $('span.npass').html('password dose`nt match');
                e.preventDefault();
            }
        });
    });
    
        </script> 
        
    </body>
</html>
 
<? ob_flush(); ?>
