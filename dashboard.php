<?php session_start();
if(!isset($_SESSION['login_admin'])) {
header("location: index.php");
exit();
}
include('php/connection.php');
include ('fb_php_sdk/config.php');
include ('fb_php_sdk/facebook.php');
$conn = mysqli_connect($host, $username, $password, $database)or die("connection lost");
?>
<!doctype html>
<html>
    <head>
        <title>Admin Page Poster</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </head>
    <body>
        <!----------------------------- Header Part -------------------------------------->
        <div class="header" style="min-height: 50px">
            <div class="row">
                <div class="logo col-md-1">
                    
                </div>
                <div class="heading col-md-8">
                    <h1 style="margin-left: 30%;">Facebook Promotion Software</h1>
                </div>
                <div class=" col-md-1" style="padding-top: 25px;">
                    <a href="logout.php" class="btn btn-primary" style="float: right">Logout</a>
                </div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
        <div class="container">
            <div class="row">
                <h3>Welcome Admin</h3>
                <div class="col-md-3 add-user">
                    <a href="adduser.php" type="button" class="btn btn-primary">ADD USER</a>
                </div>
                <div class=" col-md-3 manage-user">
                    <a href="manageuser.php" type="button" class="btn btn-primary">MANAGE USER</a>
                </div>
                <div class=" col-md-3 un-renew-user">
                    <a href="renewuser.php" type="button" class="btn btn-primary">UN RENEW USER</a>
                </div>
<!--                <div class=" col-md-3 user-dashboard">
                    <a href="dashboard.php" type="button" class="btn btn-primary">DASHBOARD</a>
                </div>-->
                <div class="col-md-12"  style="padding-top:25px">
                    <?php 
                    $id = $_GET['i'];
                    $select = "SELECT * FROM `user_registration` WHERE `user_id` = '".$id."'";
                    $result = mysqli_query($conn, $select);
                    while ($row = mysqli_fetch_array($result)) {
                        $name = $row['user_name'];
                        $email = $row['user_email'];
                        $sdate = $row['start_date'];
                        $edate = $row['end_date'];
                    }
                    $select = "SELECT `user_fb_id` FROM `user_fb_profile` WHERE `user_id` ='".$id."'";
                    $result = mysqli_query($conn, $select);
                    $user_fb_id = 0;
                    if($result){
                    while ($row = mysqli_fetch_array($result)){
                        $user_fb_id = $row['user_fb_id'];
                    }
                    }
                    ?>
                    <table class="table table-hover  table-bordered"  style="margin:1px">
                        <tr>
                            <th>USER ID</th>
                            <th>USER NAME</th>
                            <th>E-MAIL</th>
                        </tr>
                        <tr>
                            <td><?php echo $id; ?></td>
                            <td><?php echo $name; ?></td>
                            <td><?php echo $email; ?></td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <?php
            $feed_place = array();
            $date_t = array();
            $post_type = array();
            $published_t = array();
            $block_post = array();
            $delete_post = array();
            $q="SELECT * FROM `posts_details` where `user_id` = '$user_fb_id'";
            $r=  mysqli_query($conn, $q);
            $total=  mysqli_num_rows($r);
            while($row = mysqli_fetch_array($r)){
                $feed_place[] = $row['feed_place'];
                $date_t[] = $row['publish_date'];
                $post_type[] = $row['post_type'];
                $published_t[] = $row['published'];
                $block_post[] = $row['fb_post_id'];
                $delete_post[] = $row['deleted_post'];
                $pname[]=$row['place_name'];
            }
  ?>
            <div class="row">
                <h3>Total Post</h3>
                <table class="table table-hover  table-bordered"  style="margin:1px">
                    <thead>
                        <th>SR.NO</th>
                        <th>Feed Place</th>
                        <th>Feed Place Name</th>
                        <th>Date</th>
                        <th>Post Type</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        <?php
                        for($i=0;$i<$total;$i++):
                        ?>
                        <tr>
                            <td><?php echo $i+1 ?></td>
                            <td><?php echo $feed_place[$i] ?></td>
                            <td><?php echo $pname[$i] ?></td>
                            <td><?php echo $date_t[$i] ?></td>
                            <td><?php echo $post_type[$i] ?></td>
                            <td><?php if($block_post[$i]==''){
                                    echo 'Blocked';
                                }  else {
                                    echo 'Published';
                                }
                                 ?></td>
                                
                        </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>
            
            <div class="row">
                <h3>Blocked Post</h3>
                <table class="table table-hover  table-bordered"  style="margin:1px">
                    <thead>
                    <th>SR.NO</th>
                        <th>Feed Place</th>
                        <th>Feed Place Name</th>
                        <th>Date</th>
                        <th>Post Type</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        <?php $k = 0;
                        for ($i=0;$i<$total;$i++):
                            if($block_post[$i]==''): ?>
                        <tr>
                           <td><?php echo $k = $k+1 ?></td>
                            <td><?php echo $feed_place[$i] ?></td>
                            <td><?php echo $pname[$i] ?></td>
                            <td><?php echo $date_t[$i] ?></td>
                            <td><?php echo $post_type[$i] ?></td>
                            <td><?php if($published_t[$i] == 1){
                                if($block_post[$i]==''){
                                    echo 'Blocked';
                                }  else {
                                    echo 'Published';
                                }
                                 }else{
                                echo 'UnPublished'; }?></td>
                        </tr>
                        <?php endif;
                        endfor; ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <h3>Deleted Post</h3>
                <table class="table table-hover  table-bordered"  style="margin:1px">
                    <thead>
                    <th>SR.NO</th>
                        <th>Feed Place</th>
                        <th>Feed Place Name</th>
                        <th>Date</th>
                        <th>Post Type</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        <?php $k = 0;
                        for ($i=0;$i<$total;$i++):
                            if($delete_post[$i] == 1): ?>
                        <tr>
                            <td><?php echo $k = $k+1 ?></td>
                            <td><?php echo $feed_place[$i] ?></td>
                            <td><?php echo $pname[$i] ?></td>
                            <td><?php echo $date_t[$i] ?></td>
                            <td><?php echo $post_type[$i] ?></td>
                            <td>Deleted</td>
                        </tr>
                        <?php endif;
                        endfor; ?>
                    </tbody>
                </table>
            </div>
            
        </div>
    </body>
</html>
