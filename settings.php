<?php session_start();
if(!isset($_SESSION['login_admin'])) {
header("location: adminpanel.php");
exit();
}
include('php/connection.php');
$conn = mysqli_connect($host, $username, $password, $database)or die("connection lost"); ?>
<html>
    <head>
        <title>Admin Page Poster</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <!----------------------------- Header Part -------------------------------------->
         <div class="header" style="min-height: 50px">
            <div class="row">
                <div class="logo col-md-1">
                    
                </div>
                <div class="heading col-md-6">
                    <h1 style="margin-left: 30%;">Facebook Promotion Software</h1>
                </div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    <a href="manageuser.php" class="btn btn-primary">Back To Panel</a>
                </div>
                
                <div  class="col-md-12 adduser">
                    <h3 class="control-label" for="uid" style="padding-bottom: 10px">Edit Admin Panel</h3>
                    <form class="form-horizontal" role="form" action="php/config.php" method="post">
                        
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="aemail">Your E-MAIl</label>
                              <div class="col-sm-4">
                                  <input type="email" class="form-control" id="aemail" name="adminemail" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon email form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="emailreq"></span>
                              </div>
                            </div>
                        <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="password">Your Password :</label>
                              <div class="col-sm-4">
                                  <input type="password" class="form-control" id="admin-pwd" name="adminpwd" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon oldpass form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="oldpwd"></span>
                              </div>
                             </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="newpassword">Your New Password :</label>
                              <div class="col-sm-4">
                                  <input type="password" class="form-control" id="admin-npwd" name="adminnpwd" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon  npwd form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="pass"></span>
                              </div>
                            </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="newpassword">Confirm Password :</label>
                              <div class="col-sm-4">
                                  <input type="password" class="form-control" id="admin-cpwd" name="admincpwd" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon cpwd form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="npass"></span>
                              </div>
                            </div>
                        
                            <button class="btn btn-success" id="update_admin" type="submit" name="updateadmin" >UPDATE DETAILS</button>
                            <a href="manageuser.php" class="btn btn-danger">CANCEL</a>
                    </form>
                    
                    
                </div>
                <div class="col-md-12 adduser">
                    <h3>Your Mailer Settings</h3>
                    <form class="form-horizontal" role="form" action="php/config.php" method="post">
                        <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="mailer-name">Your Mailer Name :</label>
                              <div class="col-sm-4">
                                  <input type="text" class="form-control" id="mailer-name" name="m-name" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon mailer-name form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="m-name"></span>
                              </div>
                            </div>
                        <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="newpassword">Your Mailer Email:</label>
                              <div class="col-sm-4">
                                  <input type="email" class="form-control" id="mailer-email" name="mailer" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon mailer form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="m-email"></span>
                              </div>
                            </div>
                        <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="mailerhost">SMTP HOST:</label>
                              <div class="col-sm-4">
                                  <input type="text" class="form-control" id="mailer_host" name="mailerhost" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon mailerhost form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="smtphost"></span>
                              </div>
                            </div>
                        <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="smtpport">SMTP PORT:</label>
                              <div class="col-sm-4">
                                  <input type="text" class="form-control" id="smtpport" name="smtpport" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon mailerport form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="smtpport"></span>
                              </div>
                            </div>
                        <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="smtpsecure">SMTP SECURE:</label>
                              <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" name="smtpsecure" class="smtpsecure" id="smtpsecure" checked="checked" value="ssl"> SSL
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" name="smtpsecure" id="smtpsecure2" class="smtpsecure" value="tls"> TLS
                                  </label>
                              </div>
                            </div>
                        <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="message">Message:</label>
                              <div class="col-sm-4">
                                  <textarea class="form-control" id="textarea" name="message" value="" aria-describedby="inputSuccess3Status"></textarea>
                                <span class="glyphicon message form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="message"></span>
                              </div>
                            </div>
                        <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="mailerpassword">Mailer Password :</label>
                              <div class="col-sm-4">
                                  <input type="password" class="form-control" id="mailer_pwd" name="mailer-pwd" value="" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon mailer-pwd form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="p-mailer"></span>
                              </div>
                            </div>
                        <button class="btn btn-success" id="update_mailer" type="submit" name="update_mailer" >UPDATE DETAILS</button>
                            <a href="manageuser.php" class="btn btn-danger">CANCEL</a>
                    </form>
                </div>
            </div>
        </div>
        
        
        <!----------------------------- footer Part -------------------------------------->
        <script>
        /**
 * user update validation
 */
    $(document).ready(function(){
        /**
         * validation for admin form
         */
        $("#update_admin").click(function(e){
            var adminemail = $('#aemail').val();
            var oldpass = $('#admin-pwd').val();
            var newpas = $('#admin-npwd').val();
            var cpass = $('#admin-cpwd').val();
            if(adminemail == 0){
            $('span.email').addClass('glyphicon-remove');
            $('span.emailreq').html('please enter your email');
            e.preventDefault();
        } else{
            $('span.email').addClass('glyphicon-ok');
        }
         if(oldpass == 0){
                   $('span.oldpass').addClass('glyphicon-remove');
            $('span.oldpwd').html('please enter your current password');
            e.preventDefault();
        } else{
            $('span.oldpass').addClass('glyphicon-ok');
        }
        if(newpas == 0){
        $('span.npwd').addClass('glyphicon-remove');
            $('span.pass').html('please enter your new Password');
            e.preventDefault();
        }else{
            $('span.npwd').addClass('glyphicon-ok');
        }
        if(cpass == 0){
        $('span.cpwd').addClass('glyphicon-remove');
            $('span.npass').html('please Confirm your new Password');
            e.preventDefault();
        }else{
            $('span.cpwd').addClass('glyphicon-ok');
        }
        if(newpas != cpass){
            $('span.cpwd').addClass('glyphicon-remove');
            $('span.npass').html('Password Doesn`t match');
            e.preventDefault();
        }
        
    });
//        /$('#admin-pwd').keyup(validateadminpassword);
        $('#admin-pwd').change(validateadminpassword);
        function validateadminpassword(e){
            var oldpass = $('#admin-pwd').val();
                var dataString = 'a_password='+oldpass;
        if(oldpass.length>0){
        $.ajax({
        type: "POST",
        url: "ajaxcall.php",
        data: dataString,
        cache: false,
        success: function(data){
        if(data == 'true'){
           $('span.oldpass').addClass('glyphicon-ok');
           $("span.oldpwd").html("checked");
           e.preventDefault();
        }
        else{
            $("span.oldpwd").html("Error: Invalid password. ");
            e.preventDefault();
        }
        }
        });
      }  else{
            e.preventDefault();
        } 
        };
        /**
         * validation for mailer
         */
        $("#update_mailer").click(function(e){
            var name = $('#mailer-name').val();
            var email = $('#mailer-email').val();
            var port = $('#smtpport').val();
            var host = $('#mailer_host').val();
            var pass = $('#mailer_pwd').val();
            var secure = $('#smtpsecure').val();
            if(name == 0){
                   $('span.mailer-name').addClass('glyphicon-remove');
            $('span.m-name').html('please enter mailer name');
            e.preventDefault();
        } else{
            $('span.mailer-name').addClass('glyphicon-ok');
        }
        if(email == 0){
                   $('span.mailer').addClass('glyphicon-remove');
            $('span.m-email').html('please enter mailer email');
            e.preventDefault();
        } else{
            $('span.mailer').addClass('glyphicon-ok');
        }
        if(port == 0){
                   $('span.mailerport').addClass('glyphicon-remove');
            $('span.smtpport').html('Please Enter mailer port');
            e.preventDefault();
        } else{
            if($.isNumeric(port)){
            $('span.mailerport').addClass('glyphicon-ok');
        }else{
            $('span.mailerport').addClass('glyphicon-remove');
            $('span.smtpport').html('Your SMTP PORT Must be a Number');
             e.preventDefault();
        }
        }
        if(host == 0){
                   $('span.mailerhost').addClass('glyphicon-remove');
            $('span.smtphost').html('Please Enter mailer host');
            e.preventDefault();
        } else{
            $('span.mailerhost').addClass('glyphicon-ok');
        }
        if(pass == ''){
                   $('span.mailer-pwd').addClass('glyphicon-remove');
            $('span.p-mailer').html('Please Enter mailer password');
            e.preventDefault();
        } else{
            $('span.mailer-pwd').addClass('glyphicon-ok');
        }
        if(secure == ''){
                   $('span.smtp-secure').addClass('glyphicon-remove');
            $('span.smtpsecure').html('Please Enter mailer secure');
            e.preventDefault();
        } else{
            $('span.smtp-secure').addClass('glyphicon-ok');
        }
        });
    });
    
        </script> 
        
    </body>
</html>