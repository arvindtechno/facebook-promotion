<?php session_start();
if(!isset($_SESSION['login_user'])) {
header("location: index.php");
exit();
}
?>
<!DOCTYPE html>
<html>
    <head>    
        <title> Facebook Promotional Tool</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">            
        <link href="css/bootstrap.min.css" type="text/css" rel='stylesheet'/>
        <link href='css/style.css' type='text/css' rel='stylesheet' />
        <script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
        <script src="js/script.js" type="text/javascript" ></script>   
    </head>
    <body>    
        <?php include 'posts.php' ;?>
           <div class="row" id='main_div'>
            <div class='col-md-12'>
                <div class='main_content_div'>   
                    <div class="set">
                        
                        <a class="btn btn-primary" href="user-dashboard?i=<?php echo $_GET['i']; ?>">User Dashboard</a>
                        <a class="btn btn-primary" href="edit_userprofile.php?i=<?php echo $_GET['i']; ?>">Edit Profile</a>
                        <a href="logout.php" class="btn btn-primary" style="float: right">Logout</a>    
                    </div>
                <form role="form" action="" method="post">  
                    <!--=================== ########## where to post ######## ================== -->
                    <div class="set place">
                        <div class='form-group'>
                            <label class="set_head">
                                  <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                                Where you want to Post</label>
                            <div class="alert alert-warning">
                                <span style="color:#990000;" class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                Note: If you want to upload image then please first upload the image from below and
                            after that select groups or pages and write the message you want to post with it.</div>
                            <div>
                                <label class="radio-inline">
                                    <input type="radio" name="post_place" id="optionsRadios1" value="group">
                                    Group
                                </label>                 
                                <label class="radio-inline">
                                    <input type="radio" name="post_place" id="optionsRadios2" value="page">
                                    Page
                                </label>                
                                <label class="radio-inline">
                                    <input type="radio" name="post_place" id="optionsRadios3" value="wall" >
                                    Friend's Wall </label>
                                     <span style="display:inline-block;" class="pull-right"> <button id="select">Select All</button>
                                <button id="deselect">Deselect All</button></span>
                            </div>
                        </div>
                        <div id="head_place"></div>                
                        <div id="temp_place"></div>
                    </div>
                    <!-- ====================== ####### Type of post ######### =============== -->                      
                    <div class="set type">      
                        <div class='form-group'>
                            <label class="set_head">
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                Specify the type of post:</label>
                            <div>
                                <label class="radio-inline">
                                    <input type="radio" name="post_type" id="optionsRadios4" value="add_post">
                                    Write Post
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="post_type" id="optionsRadios5" value="add_photo">
                                    Add Photo
                                </label>   
                            </div>
                        </div>                
                        <div id="temp_type"></div>
                    </div>
                    <!--================= ######Write Post ##### ===================== -->                   
                        <div id="add_post">
                             <div class="set">
                            <div><label class="set_head">Please write your post:</label></div>  
                            <div class='form-group'>                     
                                <span class="block">Type your message</span>
                                <textarea class="form-control" id="post_content" name="post_content" rows="3" cols="40"></textarea>
                            </div>
                            <div class="form-group">
                                <span class="block">Share any link(optional)</span>
                                <input type="text" name="link" size="40" value="" placeholder="http://..."/>                                                    
                            </div>  
                        </div></div>
                    <!--================= ####### Adding photo##### ===================== -->                    
                        <div id="add_photo">  
                            <div class="set">
                            <div class="form-group">
                                <label class="set_head">Specify the type of Adding image:</label>
                                <div>
                                    <label class="radio-inline">
                                        <input type="radio" name="add_img_type" id="optionsRadios6" value="upload">
                                        Via Upload
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="add_img_type" id="optionsRadios7" value="url">
                                        Giving URL
                                    </label>   
                                </div></div>
                            <div id="upload">
                                <button id="clik">Upload File</button>
                                <?php if(isset($_SESSION['img'])){ ?><span style="display: inline-block"><img height="80" width="120" style="padding:5px;" src="temp_img/<?php echo "$_SESSION[img]" ?>" /></span><?php } ?>
                                 <div class='form-group'>                     
                                <span class="block">Type your message(Optional, not required)</span>
                                <textarea class="form-control" id="post_content_img1" name="post_content_img1" rows="3" cols="40"></textarea>
                            </div>
                               </div>                                
                            <div id="url">
                                <div class="form-group">
                                    <span class="block">Please give the complete URL path of image</span>
                                    <input type="text" name="input_url" size="40" value="" placeholder="http://..."/>
                                </div>
                                 <div class='form-group'>                     
                                <span class="block">Type your message(Optional, not required)</span>
                                <textarea class="form-control" id="post_content_img2" name="post_content_img2" rows="3" cols="40"></textarea>
                            </div>
                            </div>
                        </div>
                    </div>                  
                   <!---============#########Time gap for each post ######====== -->                  
                    <div class="set">
                        <label class="set_head">
                             <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                            Enter the time gap for each post </label> 
                        <span class='light'>
                             <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span>
                            Note: It is not required. You can specify if you have selected more than one groups or pages(minimum gap should be 15 minutes)</span>
                        <div class="form-group">       
                            <span class='block'>Enter the time gap to be applied</span>
                            <input name='single_gap' type="text" size="20" value="" placeholder="Enter digits"/>  
                            <select name='single_gap_unit'>                               
                                <option>minutes</option>
                                <option>hours</option>
                            </select>
                        </div>
                    </div>
                    <!---============#########Time gap for between bulk posts ######====== -->
                    <div class="set">
                        <label class="set_head">
                             <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                            Enter the time gap between bulk posts </label>
                        <span class='light'> 
                              <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span>
                            Note: It is not required. You can specify if you have selected more than three groups or pages</span>
                       <div class="form-group"> 
                           <span class='block'>After how many posts this time gap will be applied</span>
                            <input name='bulk_no' type="text" size="20" value="" placeholder="Enter digits"/>  
                       </div>
                        <div class="form-group">    
                            <span class='block'>Enter the time gap to be applied</span>
                            <input name='bulk_gap' type="text" size="20" value="" placeholder="Enter digits"/>  
                            <select name='multiple_gap_unit'>                               
                                <option>minutes</option>
                                <option>hours</option>
                            </select>
                        </div>
                    </div>               
                    <!--================= ####### submit ######### ===========================-->
                    <div class='form-group'>                        
                       <input name='submit' type="submit" value="Submit" class="btn btn-primary" />               
                    </div>                                      
                </form>    
                </div>
            </div>       
        </div>
         <div class='row'> 
            <div id="fb-root">                 
                </div>
            </div>
       
    </body>
</html>