<?php session_start();
if(!isset($_SESSION['login_admin'])) {
header("location: index.php");
exit();
}
include('php/connection.php');
$conn = mysqli_connect($host, $username, $password, $database)or die("connection lost");
?>
<!doctype html>
<html>
    <head>
        <title>Admin Page Poster</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </head>
    <body>
        <!----------------------------- Header Part -------------------------------------->
         <div class="header" style="min-height: 50px">
            <div class="row">
                <div class="logo col-md-1">
                    
                </div>
                <div class="heading col-md-6">
                    <h1 style="margin-left: 30%;">Facebook Promotion Software</h1>
                </div>
                <div class=" col-md-1" style="padding-top: 25px;">
                    <a href="logout.php" class="btn btn-primary" style="float: right">Logout</a>
                </div>
                 <div class="col-md-4" style="padding-top:25px">
                    <form class="form-inline" role="form" action="search.php" method="post">
                            
                            <div class="form-group has-success has-feedback">
                                <input type="text" name="search" class="form-control" id="search-user" placeholder="Search By Name">
                                <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                            </div>
                        <button type="submit" name="search_user" class="btn btn-info">SEARCH</button>
                             
                    </form>
                </div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
        <div class="container">
            <div class="row">
                <h3>Welcome Admin</h3>
                <div class="col-md-3 add-user">
                    <a href="adduser.php" type="button" class="btn btn-primary">ADD USER</a>
                </div>
                <div class=" col-md-3 manage-user">
                    <a href="manageuser.php" type="button" class="btn btn-primary">MANAGE USER</a>
                </div>
                <div class=" col-md-3 un-renew-user">
                    <a href="renewuser.php" type="button" class="btn btn-primary">UN RENEW USER</a>
                </div>
            <div  class="col-md-12 adduser">
                    <h3 class="control-label" for="uid" style="padding-bottom: 10px">Edit User Pannel</h3>
                        
                    <?php
                    $id = $_GET['i'];
                    $select = "SELECT * FROM `user_registration` WHERE `user_id` = '".$id."'";
                    $result = mysqli_query($conn, $select);
                    while ($row = mysqli_fetch_array($result)) {
                        $name = $row['user_name'];
                        $email = $row['user_email'];
                        $sdate = $row['start_date'];
                        $edate = $row['end_date'];
                        
                    }
                    $profile = array();
                    $select = "SELECT * FROM `user_fb_profile` WHERE `user_id` = '".$id."'";
                    $result = mysqli_query($conn, $select);
                    while ($row = mysqli_fetch_array($result)) {
                        $profile[$row['profile_id']] = $row['profile_link'];
                    }
                    ?>
                    
                    
                    <form class="form-horizontal" role="form" action="php/config.php" method="post">
                        
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="uname">USER NAME</label>
                              <div class="col-sm-4">
                                  <input type="text" class="form-control" id="uname" name="uname" value="<?php echo $name ?>" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon name form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="namereq"></span>
                              </div>
                            </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="uemail">USER E-MAIl</label>
                              <div class="col-sm-4">
                                  <input type="email" class="form-control" id="uemail" name="uemail" value="<?php echo $email ?>" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon email form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="emailreq"></span>
                              </div>
                            </div>
                        
                        <?php foreach ($profile as $key=>$value): ?>
                                <div class="form-group has-success has-feedback fb-profile">
                              <label class="control-label col-sm-2" for="add_profile">ADD FB PROFILE</label>
                              <div class="col-sm-4">
                                  <input type="hidden" name="pro-id[]" value="<?php echo $key ?>"/>
                                  <input type="text" class="form-control" id="add_profile" name="add_profile[]" value="<?php echo $value ?>" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon fbprofile form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="profilereq"></span>
                              </div>
                            </div>
                        <?php endforeach; ?>
                                <div id="row1">

                                </div>
                            <button class="btn btn-info" id="add_more" >ADD MORE PROFILE</button>
                            <input class="btn btn-danger" id="remove_field" type="hidden" value="REMOVE FIELD"/>
                             <div class="form-group has-success has-feedback">
                                <h4>Duration</h4>
                              <label class="control-label col-sm-2" for="start_date">START DATE</label>
                              <div class="col-sm-4">
                                  <input type="date" class="form-control" id="start_date" name="sdate" value="<?php echo $sdate ?>" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon sdate form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="sdatereq"></span>
                              </div>
                             </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="end_date">END DATE</label>
                              <div class="col-sm-4">
                                  <input type="date" class="form-control" id="end_date" name="edate" value="<?php echo $edate ?>" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon edate form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="edatereq"></span>
                              </div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id ?>"/>
                            <button class="btn btn-success" id="registration" type="submit" name="edit_user_registration" >UPDATE DETAILS</button>
                            <a href="manageuser.php" class="btn btn-danger">CANCEL</a>
                    </form>
                    
                    
                </div>
            </div>
        </div>
        
        
        <!----------------------------- footer Part -------------------------------------->
        
        
    </body>
</html>
        

<? ob_flush(); ?>