<?php session_start();
if(!isset($_SESSION['login_admin'])) {
header("location: adminpanel.php");
exit();
}
?>
<!doctype html>
<html>
    <head>
        <title>Admin Page Poster</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </head>
    <body>
        <!----------------------------- Header Part -------------------------------------->
         <div class="header" style="min-height: 50px">
            <div class="row">
                <div class="logo col-md-1">
                    
                </div>
                <div class="heading col-md-6">
                    <h1 style="margin-left: 30%;">Facebook Promotion Software</h1>
                </div>
                <div class=" col-md-1" style="padding-top: 25px;">
                    <a href="logout.php" class="btn btn-primary" style="float: right">Logout</a>
                </div>
                 <div class="col-md-4" style="padding-top:25px">
                    <form class="form-inline" role="form" action="search.php" method="post">
                            
                            <div class="form-group has-success has-feedback">
                                <input type="text" name="search" class="form-control" id="search-user" placeholder="Search By Name">
                                <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                            </div>
                        <button type="submit" name="search_user" class="btn btn-info">SEARCH</button>
                             
                    </form>
                     
                </div>
            </div>
        </div>
        <!----------------------------- Content Part -------------------------------------->
        <div class="container">
            <div class="row">
                <h3>Welcome Admin</h3>
                <div class="col-md-3 add-user">
                    <a href="adduser.php" type="button" class="btn btn-primary">ADD USER</a>
                </div>
                <div class=" col-md-3 manage-user">
                    <a href="manageuser.php" type="button" class="btn btn-primary">MANAGE USER</a>
                </div>
                <div class=" col-md-3 un-renew-user">
                    <a href="renewuser.php" type="button" class="btn btn-primary">UN RENEW USER</a>
                </div>
<!--                <div class=" col-md-3 user-dashboard">
                    <a href="dashboard.php" type="button" class="btn btn-primary">DASHBOARD</a>
                </div>-->
                <div class="col-md-12">
                    
                </div>
            </div>
        </div>
        
        
        <!----------------------------- footer Part -------------------------------------->
        
        
    </body>
</html>
        
<? ob_flush(); ?>