<?php
date_default_timezone_set('asia/kolkata');
include ('fb_php_sdk/config.php');
include ('fb_php_sdk/facebook.php');
include ('php/connection.php');
$conn = mysqli_connect($host, $username, $password, $database)or die("connection lost");
$facebook = new Facebook(array(
    'appId' => APP_ID,
    'secret' => APP_SECRET,
    'fileUpload' => true,
        ));
Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;
Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;

$user = $facebook->getUser();
if ($user) { 
    echo '<div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>';
    echo ' You can logout <a id="logout_btn" href="logout.php"><span style="border-bottom:1px solid black">here.</span></a></div>';
    $facebook->setExtendedAccessToken();
    $tmp = $facebook->getAccessToken();   
    if(!isset($_GET['u'])){
        unset($_SESSION['img']);
    }  
    if(isset($_GET['i'])){
    $lid=$_GET['i'];      
    $_SESSION['ulid']=$lid;
   $qq="UPDATE user_fb_profile SET user_fb_id='$user' , user_access_token='$tmp' WHERE user_id='$lid'";
    $rr=  mysqli_query($conn, $qq);
    }
    $select = "SELECT * FROM `user_registration` WHERE `user_id` = $lid";
                    $result=  mysqli_query($conn, $select);
                    while ($row = mysqli_fetch_array($result)){
                        $date1 = date_create();
                            $date2 = date_create($row['end_date']);
                            $diff=date_diff($date1,$date2);
                            $day = $diff->format("%R%a");
                    }
                    if($day <= -1){
                        echo '<div class="alert alert-danger"><span style="color:#990000;" class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>Sorry Your Limit Has Been Expired..</div>';
                    }else{
    ///////////////////store img file
    if (isset($_FILES['local_file'])) {        
        $photo = $_FILES['local_file']['tmp_name'];
        $name = $_FILES['local_file']['name'];
         $ext1 = pathinfo($name, PATHINFO_EXTENSION);
                   $ext=strtolower($ext1);                  
                   if($ext != 'jpg' && $ext != 'jpeg' && $ext != 'png' && $ext != 'gif'){
                         echo '<div class="alert alert-danger">Please select image format of jpg, png or gif!!</div>';
                         return false;
                   }
        $type = $_FILES['local_file']['type'];
        $dir = "temp_img/";
        $target_file = $dir . basename($_FILES["local_file"]["name"]);
        move_uploaded_file($photo, $target_file);
        $_SESSION['img']=$name;
       echo '<div class="alert alert-success">Your image has selected, please select groups or pages now and write any message in upload textbox if you want..</div>'; 
    }
   
    // DB insertion
    if (isset($_POST['submit'])) {   
         if ((isset($_POST['add_img_type'])) && ($_POST['add_img_type']) == 'upload') {
                    if (!isset($_POST['groups']) && !isset($_POST['pages']) && !isset($_POST['friends'])) {
                        echo '<div class="alert alert-danger">Please select any group or page etc to post..!!</div>';
                        return false;
                    }
         }
        if (isset($_POST['groups'])) {
         $totalno=count($_POST['groups']);
          $tno=1;
            $i = 0;
            foreach ($_POST['groups'] as $grp) {  
                ////////////////////////////////write post/////////////////////////////
                /////////////////////////////////////////////////////////////////////////
                if ((isset($_POST['post_type'])) && $_POST['post_type'] == 'add_post') {
                    $msg = $_POST['post_content'];                   
                    $link = $_POST['link'];
                    $type = 'write_post';
                    $feed_place='group';
                     if($msg=='' && $link==''){
        echo 'Please enter the message to post!!';
        return false;
    }  
                    // $today = date("Y-m-d H:i:s"); 
                    //  $date=datetime::createFromFormat('Y-m-d H:i:s', $today);  
                    $date = new DateTime('now');
                    $dur = $_POST['single_gap'];
                    $t_dur = $i * $dur;
                    $bulk_dur = $_POST['bulk_gap'];
                    $bulk_no = $_POST['bulk_no'];
                    if ($dur === '' && $bulk_dur === '') {
                        $pub_time = $date;                       
                    } 
                   else{                                          
                        if ($bulk_dur === '' || $bulk_no === '') {                           
                            $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                        } else {
                            if ($i % $bulk_no === 0 && $i!=0) {
                                $unit = $_POST['single_gap_unit'];
                                $units = $_POST['multiple_gap_unit'];
                                if ($units === $unit) {
                                    $t_dur+=$bulk_dur;                                    
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                                    }
                                } else {
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $bulk_dur . 'H' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H' . $bulk_dur . 'M'));
                                    }
                                }
                            }else{
                                 $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                            }
                        }
                    }
                    $lpub_time=$pub_time->format('Y-m-d H:i:s');                    
                     $q = "INSERT INTO submit_fb_posts(user_id,link,msg,post_type,feed_place_id,publish_time) VALUES('$user','$link ',' $msg ',' $type ',' $grp ',' $lpub_time ')";
                      $r = mysqli_query($conn,$q);
                     // echo $q;
                      //var_dump($r);
                    if ($r) {
                       // echo 'values inserted';
                        $q1='SELECT * FROM submit_fb_posts ORDER BY local_post_id DESC LIMIT 1';
                        $r1 = mysqli_query($conn,$q1);
                        $row=  mysqli_fetch_row($r1);
                       $local_id=$row[0];                      
                    } else {                      
                        echo 'Sorry, an error occurred please try again...';
                    }
 $pname=$_POST[$grp];
                    $q3 = "INSERT INTO posts_details(user_id,local_post_id,publish_date,feed_place,feed_place_id,post_type,place_name) VALUES('$user','$local_id','$lpub_time ','$feed_place','$grp','$type','$pname')";                  
                    $r3 = mysqli_query($conn,$q3);
                    if (!$r3) {                       
                        echo 'Sorry, an error occurred please try again...';
                    }
                      echo '<div class="alert alert-success">Your post '.($i+1).' will be posted at '.$lpub_time.'</div>'; 
                }
                ///////////////////////////////     URL               /////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////
                if ((isset($_POST['add_img_type'])) && ($_POST['add_img_type']) == 'url') {
                     $msg = $_POST['post_content_img2'];                   
                    $link = $_POST['input_url'];
                    $type = 'giving_url';
                    $feed_place='group';
                     if($link==''){
        echo 'Please enter the image link to post!!';
        return false;
    }  
                    // $today = date("Y-m-d H:i:s"); 
                    //  $date=datetime::createFromFormat('Y-m-d H:i:s', $today);  
                    $date = new DateTime('now');
                    $dur = $_POST['single_gap'];
                    $t_dur = $i * $dur;
                    $bulk_dur = $_POST['bulk_gap'];
                    $bulk_no = $_POST['bulk_no'];
                    if ($dur === '' && $bulk_dur === '') {
                        $pub_time = $date;                       
                    } 
                   else{                                          
                        if ($bulk_dur === '' || $bulk_no === '') {                           
                            $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                        } else {
                            if ($i % $bulk_no === 0 && $i!=0) {
                                $unit = $_POST['single_gap_unit'];
                                $units = $_POST['multiple_gap_unit'];
                                if ($units === $unit) {
                                    $t_dur+=$bulk_dur;                                    
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                                    }
                                } else {
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $bulk_dur . 'H' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H' . $bulk_dur . 'M'));
                                    }
                                }
                            }else{
                                 $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                            }
                        }
                    }
                    $lpub_time=$pub_time->format('Y-m-d H:i:s');                   
                     $q = "INSERT INTO submit_fb_posts(user_id,link,msg,post_type,feed_place_id,publish_time) VALUES('$user','$link ',' $msg ',' $type ',' $grp ',' $lpub_time ')";
                      $r = mysqli_query($conn,$q);
                     // echo $q;
                      //var_dump($r);
                    if ($r) {
                       // echo 'values inserted';
                        $q1='SELECT * FROM submit_fb_posts ORDER BY local_post_id DESC LIMIT 1';
                        $r1 = mysqli_query($conn,$q1);
                        $row=  mysqli_fetch_row($r1);
                       $local_id=$row[0];                      
                    } else {                      
                        echo 'Sorry, an error occurred please try again...';
                    }
 $pname=$_POST[$grp];
                    $q3 = "INSERT INTO posts_details(user_id,local_post_id,publish_date,feed_place,feed_place_id,post_type,place_name) VALUES('$user','$local_id','$lpub_time ','$feed_place','$grp','$type','$pname')";                  
                    $r3 = mysqli_query($conn,$q3);
                    if (!$r3) {                       
                        echo 'Sorry, an error occurred please try again...';
                    }
                      echo '<div class="alert alert-success">Your post '.($i+1).' will be posted at '.$lpub_time.'</div>'; 
                }
                ///////////////////////////////////UPLOAD/////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////
                if(isset($_SESSION['img'])) {    
                    
                     $msg = $_POST['post_content_img1'];                   
                    $link = $_SESSION['img'];                                    
                    //unset($_SESSION['img']);
                    $type = 'via_upload';
                    $feed_place='group';
                     if($link==''){
        echo 'Please upload the img to post!!';
         unset($_SESSION['img']);
        return false;
    }  
        if($tno>=$totalno){
                        unset($_SESSION['img']);
                    }     
                    // $today = date("Y-m-d H:i:s"); 
                    //  $date=datetime::createFromFormat('Y-m-d H:i:s', $today);  
                    $date = new DateTime('now');
                    $dur = $_POST['single_gap'];
                    $t_dur = $i * $dur;
                    $bulk_dur = $_POST['bulk_gap'];
                    $bulk_no = $_POST['bulk_no'];
                    if ($dur === '' && $bulk_dur === '') {
                        $pub_time = $date;                       
                    } 
                   else{                                          
                        if ($bulk_dur === '' || $bulk_no === '') {                           
                            $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                        } else {
                            if ($i % $bulk_no === 0 && $i!=0) {
                                $unit = $_POST['single_gap_unit'];
                                $units = $_POST['multiple_gap_unit'];
                                if ($units === $unit) {
                                    $t_dur+=$bulk_dur;                                    
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                                    }
                                } else {
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $bulk_dur . 'H' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H' . $bulk_dur . 'M'));
                                    }
                                }
                            }else{
                                 $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                            }
                        }
                    }
                    $lpub_time=$pub_time->format('Y-m-d H:i:s');                   
                     $q = "INSERT INTO submit_fb_posts(user_id,link,msg,post_type,feed_place_id,publish_time) VALUES('$user','$link ',' $msg ',' $type ',' $grp ',' $lpub_time ')";
                      $r = mysqli_query($conn,$q);
                     // echo $q;
                      //var_dump($r);
                    if ($r) {
                       // echo 'values inserted';
                        $q1='SELECT * FROM submit_fb_posts ORDER BY local_post_id DESC LIMIT 1';
                        $r1 = mysqli_query($conn,$q1);
                        $row=  mysqli_fetch_row($r1);
                       $local_id=$row[0];                      
                    } else {                      
                        echo 'Sorry, an error occurred please try again...';
                    }
 $pname=$_POST[$grp];
                    $q3 = "INSERT INTO posts_details(user_id,local_post_id,publish_date,feed_place,feed_place_id,post_type,place_name) VALUES('$user','$local_id','$lpub_time ','$feed_place','$grp','$type','$pname')";                  
                    $r3 = mysqli_query($conn,$q3);
                    if (!$r3) {                       
                        echo 'Sorry, an error occurred please try again...';
                    }
                      echo '<div class="alert alert-success">Your post '.($i+1).' will be posted at '.$lpub_time.'</div>'; 
                }
                $i++;
                $tno++;
            }
        }

        if (isset($_POST['pages'])) {
         $totalno=count($_POST['pages']);
          $tno=1;
             $i = 0;
            foreach ($_POST['pages'] as $pg) {  
                ////////////////////////////////write post/////////////////////////////
                /////////////////////////////////////////////////////////////////////////
                if ((isset($_POST['post_type'])) && $_POST['post_type'] == 'add_post') {
                    $msg = $_POST['post_content'];                   
                    $link = $_POST['link'];
                    $type = 'write_post';
                    $feed_place='page';
                     if($msg=='' && $link==''){
        echo 'Please enter the message to post!!';
        return false;
    }  
                    // $today = date("Y-m-d H:i:s"); 
                    //  $date=datetime::createFromFormat('Y-m-d H:i:s', $today);  
                    $date = new DateTime('now');
                    $dur = $_POST['single_gap'];
                    $t_dur = $i * $dur;
                    $bulk_dur = $_POST['bulk_gap'];
                    $bulk_no = $_POST['bulk_no'];
                    if ($dur === '' && $bulk_dur === '') {
                        $pub_time = $date;                       
                    } 
                   else{                                          
                        if ($bulk_dur === '' || $bulk_no === '') {                           
                            $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                        } else {
                            if ($i % $bulk_no === 0 && $i!=0) {
                                $unit = $_POST['single_gap_unit'];
                                $units = $_POST['multiple_gap_unit'];
                                if ($units === $unit) {
                                    $t_dur+=$bulk_dur;                                    
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                                    }
                                } else {
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $bulk_dur . 'H' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H' . $bulk_dur . 'M'));
                                    }
                                }
                            }else{
                                 $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                            }
                        }
                    }
                    $lpub_time=$pub_time->format('Y-m-d H:i:s');                   
                     $q = "INSERT INTO submit_fb_posts(user_id,link,msg,post_type,feed_place_id,publish_time) VALUES('$user','$link ',' $msg ',' $type ',' $pg ',' $lpub_time ')";
                      $r = mysqli_query($conn,$q);
                     // echo $q;
                      //var_dump($r);
                    if ($r) {
                       // echo 'values inserted';
                        $q1='SELECT * FROM submit_fb_posts ORDER BY local_post_id DESC LIMIT 1';
                        $r1 = mysqli_query($conn,$q1);
                        $row=  mysqli_fetch_row($r1);
                       $local_id=$row[0];                      
                    } else {                      
                        echo 'Sorry, an error occurred please try again...';
                    }
 $pname=$_POST[$grp];
                    $q3 = "INSERT INTO posts_details(user_id,local_post_id,publish_date,feed_place,feed_place_id,post_type,place_name) VALUES('$user','$local_id','$lpub_time ','$feed_place','$pg','$type','$pname')";                  
                    $r3 = mysqli_query($conn,$q3);
                    if (!$r3) {                       
                        echo 'Sorry, an error occurred please try again...';
                    }
                      echo '<div class="alert alert-success">Your post '.($i+1).' will be posted at '.$lpub_time.'</div>'; 
                }
                ///////////////////////////////     URL               /////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////
                if ((isset($_POST['add_img_type'])) && ($_POST['add_img_type']) == 'url') {
                     $msg = $_POST['post_content_img2'];                   
                    $link = $_POST['input_url'];
                    $type = 'giving_url';
                    $feed_place='page';
                     if($link==''){
        echo 'Please enter the image link to post!!';
        return false;
    }  
                    // $today = date("Y-m-d H:i:s"); 
                    //  $date=datetime::createFromFormat('Y-m-d H:i:s', $today);  
                    $date = new DateTime('now');
                    $dur = $_POST['single_gap'];
                    $t_dur = $i * $dur;
                    $bulk_dur = $_POST['bulk_gap'];
                    $bulk_no = $_POST['bulk_no'];
                    if ($dur === '' && $bulk_dur === '') {
                        $pub_time = $date;                       
                    } 
                   else{                                          
                        if ($bulk_dur === '' || $bulk_no === '') {                           
                            $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                        } else {
                            if ($i % $bulk_no === 0 && $i!=0) {
                                $unit = $_POST['single_gap_unit'];
                                $units = $_POST['multiple_gap_unit'];
                                if ($units === $unit) {
                                    $t_dur+=$bulk_dur;                                    
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                                    }
                                } else {
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $bulk_dur . 'H' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H' . $bulk_dur . 'M'));
                                    }
                                }
                            }else{
                                 $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                            }
                        }
                    }
                    $lpub_time=$pub_time->format('Y-m-d H:i:s');                   
                     $q = "INSERT INTO submit_fb_posts(user_id,link,msg,post_type,feed_place_id,publish_time) VALUES('$user','$link ',' $msg ',' $type ',' $pg ',' $lpub_time ')";
                      $r = mysqli_query($conn,$q);
                     // echo $q;
                      //var_dump($r);
                    if ($r) {
                       // echo 'values inserted';
                        $q1='SELECT * FROM submit_fb_posts ORDER BY local_post_id DESC LIMIT 1';
                        $r1 = mysqli_query($conn,$q1);
                        $row=  mysqli_fetch_row($r1);
                       $local_id=$row[0];                      
                    } else {                      
                        echo 'Sorry, an error occurred please try again...';
                    }
 $pname=$_POST[$grp];
                    $q3 = "INSERT INTO posts_details(user_id,local_post_id,publish_date,feed_place,feed_place_id,post_type,place_name) VALUES('$user','$local_id','$lpub_time ','$feed_place','$pg','$type','$pname')";                  
                    $r3 = mysqli_query($conn,$q3);
                    if (!$r3) {                       
                        echo 'Sorry, an error occurred please try again...';
                    }
                      echo '<div class="alert alert-success">Your post '.($i+1).' will be posted at '.$lpub_time.'</div>'; 
                }
                ///////////////////////////////////UPLOAD/////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////
                 if(isset($_SESSION['img'])) {      
                     $msg = $_POST['post_content_img1'];                   
                    $link = $_SESSION['img'];
                   // unset($_SESSION['img']);
                    $type = 'via_upload';
                    $feed_place='page';
                     if($link==''){
        echo 'Please upload the img to post!!';
        unset($_SESSION['img']);
        return false;
    }  
      if($tno>=$totalno){
                        unset($_SESSION['img']);
                    }
                    // $today = date("Y-m-d H:i:s"); 
                    //  $date=datetime::createFromFormat('Y-m-d H:i:s', $today);  
                    $date = new DateTime('now');
                    $dur = $_POST['single_gap'];
                    $t_dur = $i * $dur;
                    $bulk_dur = $_POST['bulk_gap'];
                    $bulk_no = $_POST['bulk_no'];
                    if ($dur === '' && $bulk_dur === '') {
                        $pub_time = $date;                       
                    } 
                   else{                                          
                        if ($bulk_dur === '' || $bulk_no === '') {                           
                            $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                        } else {
                            if ($i % $bulk_no === 0 && $i!=0) {
                                $unit = $_POST['single_gap_unit'];
                                $units = $_POST['multiple_gap_unit'];
                                if ($units === $unit) {
                                    $t_dur+=$bulk_dur;                                    
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                                    }
                                } else {
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $bulk_dur . 'H' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H' . $bulk_dur . 'M'));
                                    }
                                }
                            }else{
                                 $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                            }
                        }
                    }
                    $lpub_time=$pub_time->format('Y-m-d H:i:s');                   
                     $q = "INSERT INTO submit_fb_posts(user_id,link,msg,post_type,feed_place_id,publish_time) VALUES('$user','$link ',' $msg ',' $type ',' $pg ',' $lpub_time ')";
                      $r = mysqli_query($conn,$q);
                     // echo $q;
                      //var_dump($r);
                    if ($r) {
                       // echo 'values inserted';
                        $q1='SELECT * FROM submit_fb_posts ORDER BY local_post_id DESC LIMIT 1';
                        $r1 = mysqli_query($conn,$q1);
                        $row=  mysqli_fetch_row($r1);
                       $local_id=$row[0];                      
                    } else {                      
                        echo 'Sorry, an error occurred please try again...';
                    }
 $pname=$_POST[$grp];
                    $q3 = "INSERT INTO posts_details(user_id,local_post_id,publish_date,feed_place,feed_place_id,post_type,place_name) VALUES('$user','$local_id','$lpub_time ','$feed_place','$pg','$type','$pname')";                  
                    $r3 = mysqli_query($conn,$q3);
                    if (!$r3) {                       
                        echo 'Sorry, an error occurred please try again...';
                    }
                      echo '<div class="alert alert-success">Your post '.($i+1).' will be posted at '.$lpub_time.'</div>'; 
                }
                $i++;
                $tno++;
            }
        }
        if (isset($_POST['friends'])) {
             $i = 0;
              $totalno=count($_POST['friends']);
            $tno=1;
            foreach ($_POST['friends'] as $frnd) { 
            
                ////////////////////////////////write post/////////////////////////////
                /////////////////////////////////////////////////////////////////////////
                if ((isset($_POST['post_type'])) && $_POST['post_type'] == 'add_post') {
                    $msg = $_POST['post_content'];                   
                    $link = $_POST['link'];
                    $type = 'write_post';
                    $feed_place='friend_wall';
                     if($msg=='' && $link==''){
        echo 'Please enter the message to post!!';
        return false;
    }  
                    // $today = date("Y-m-d H:i:s"); 
                    //  $date=datetime::createFromFormat('Y-m-d H:i:s', $today);  
                    $date = new DateTime('now');
                    $dur = $_POST['single_gap'];
                    $t_dur = $i * $dur;
                    $bulk_dur = $_POST['bulk_gap'];
                    $bulk_no = $_POST['bulk_no'];
                    if ($dur === '' && $bulk_dur === '') {
                        $pub_time = $date;                       
                    } 
                   else{                                          
                        if ($bulk_dur === '' || $bulk_no === '') {                           
                            $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                        } else {
                            if ($i % $bulk_no === 0 && $i!=0) {
                                $unit = $_POST['single_gap_unit'];
                                $units = $_POST['multiple_gap_unit'];
                                if ($units === $unit) {
                                    $t_dur+=$bulk_dur;                                    
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                                    }
                                } else {
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $bulk_dur . 'H' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H' . $bulk_dur . 'M'));
                                    }
                                }
                            }else{
                                 $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                            }
                        }
                    }
                    $lpub_time=$pub_time->format('Y-m-d H:i:s');                   
                     $q = "INSERT INTO submit_fb_posts(user_id,link,msg,post_type,feed_place_id,publish_time) VALUES('$user','$link ',' $msg ',' $type ',' $frnd ',' $lpub_time ')";
                      $r = mysqli_query($conn,$q);
                     // echo $q;
                      //var_dump($r);
                    if ($r) {
                       // echo 'values inserted';
                        $q1='SELECT * FROM submit_fb_posts ORDER BY local_post_id DESC LIMIT 1';
                        $r1 = mysqli_query($conn,$q1);
                        $row=  mysqli_fetch_row($r1);
                       $local_id=$row[0];                      
                    } else {                      
                        echo 'Sorry, an error occurred please try again...';
                    }
 $pname=$_POST[$grp];
                    $q3 = "INSERT INTO posts_details(user_id,local_post_id,publish_date,feed_place,feed_place_id,post_type,place_name) VALUES('$user','$local_id','$lpub_time ','$feed_place','$frnd','$type','$pname')";                  
                    $r3 = mysqli_query($conn,$q3);
                    if (!$r3) {                       
                        echo 'Sorry, an error occurred please try again...';
                    }
                      echo '<div class="alert alert-success">Your post '.($i+1).' will be posted at '.$lpub_time.'</div>'; 
                }
                ///////////////////////////////     URL               /////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////
                if ((isset($_POST['add_img_type'])) && ($_POST['add_img_type']) == 'url') {
                     $msg = $_POST['post_content_img2'];                   
                    $link = $_POST['input_url'];
                    $type = 'giving_url';
                    $feed_place='friend_wall';
                     if($link==''){
        echo 'Please enter the image link to post!!';
        return false;
    }  
                    // $today = date("Y-m-d H:i:s"); 
                    //  $date=datetime::createFromFormat('Y-m-d H:i:s', $today);  
                    $date = new DateTime('now');
                    $dur = $_POST['single_gap'];
                    $t_dur = $i * $dur;
                    $bulk_dur = $_POST['bulk_gap'];
                    $bulk_no = $_POST['bulk_no'];
                    if ($dur === '' && $bulk_dur === '') {
                        $pub_time = $date;                       
                    } 
                   else{                                          
                        if ($bulk_dur === '' || $bulk_no === '') {                           
                            $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                        } else {
                            if ($i % $bulk_no === 0 && $i!=0) {
                                $unit = $_POST['single_gap_unit'];
                                $units = $_POST['multiple_gap_unit'];
                                if ($units === $unit) {
                                    $t_dur+=$bulk_dur;                                    
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                                    }
                                } else {
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $bulk_dur . 'H' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H' . $bulk_dur . 'M'));
                                    }
                                }
                            }else{
                                 $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                            }
                        }
                    }
                    $lpub_time=$pub_time->format('Y-m-d H:i:s');                   
                     $q = "INSERT INTO submit_fb_posts(user_id,link,msg,post_type,feed_place_id,publish_time) VALUES('$user','$link ',' $msg ',' $type ',' $frnd ',' $lpub_time ')";
                      $r = mysqli_query($conn,$q);
                     // echo $q;
                      //var_dump($r);
                    if ($r) {
                       // echo 'values inserted';
                        $q1='SELECT * FROM submit_fb_posts ORDER BY local_post_id DESC LIMIT 1';
                        $r1 = mysqli_query($conn,$q1);
                        $row=  mysqli_fetch_row($r1);
                       $local_id=$row[0];                      
                    } else {                      
                        echo 'Sorry, an error occurred please try again...';
                    }
 $pname=$_POST[$grp];
                    $q3 = "INSERT INTO posts_details(user_id,local_post_id,publish_date,feed_place,feed_place_id,post_type,place_name) VALUES('$user','$local_id','$lpub_time ','$feed_place','$frnd','$type','$pname')";                  
                    $r3 = mysqli_query($conn,$q3);
                    if (!$r3) {                       
                        echo 'Sorry, an error occurred please try again...';
                    }
                      echo '<div class="alert alert-success">Your post '.($i+1).' will be posted at '.$lpub_time.'</div>'; 
                }
                ///////////////////////////////////UPLOAD/////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////
                 if(isset($_SESSION['img'])) {                       
                     $msg = $_POST['post_content_img1'];                   
                    $link = $_SESSION['img'];                   
                   // unset($_SESSION['img']);
                    $type = 'via_upload';
                    $feed_place='friend_wall';
                     if($link==''){
        echo 'Please upload the img to post!!';
         unset($_SESSION['img']);
        return false;
    }  
    if($tno>=$totalno){
                        unset($_SESSION['img']);
                    }
                    // $today = date("Y-m-d H:i:s"); 
                    //  $date=datetime::createFromFormat('Y-m-d H:i:s', $today);  
                    $date = new DateTime('now');
                    $dur = $_POST['single_gap'];
                    $t_dur = $i * $dur;
                    $bulk_dur = $_POST['bulk_gap'];
                    $bulk_no = $_POST['bulk_no'];
                    if ($dur === '' && $bulk_dur === '') {
                        $pub_time = $date;                       
                    } 
                   else{                                          
                        if ($bulk_dur === '' || $bulk_no === '') {                           
                            $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                        } else {
                            if ($i % $bulk_no === 0 && $i!=0) {
                                $unit = $_POST['single_gap_unit'];
                                $units = $_POST['multiple_gap_unit'];
                                if ($units === $unit) {
                                    $t_dur+=$bulk_dur;                                    
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                                    }
                                } else {
                                    if ($unit === 'minutes') {
                                        $pub_time = $date->add(new DateInterval('PT' . $bulk_dur . 'H' . $t_dur . 'M'));
                                    }
                                    if ($unit === 'hours') {
                                        $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H' . $bulk_dur . 'M'));
                                    }
                                }
                            }else{
                                 $unit = $_POST['single_gap_unit'];
                            if ($unit === 'minutes') {                               
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'M'));
                            }
                            if ($unit === 'hours') {
                                $pub_time = $date->add(new DateInterval('PT' . $t_dur . 'H'));
                            }
                            }
                        }
                    }
                    $lpub_time=$pub_time->format('Y-m-d H:i:s');                   
                     $q = "INSERT INTO submit_fb_posts(user_id,link,msg,post_type,feed_place_id,publish_time) VALUES('$user','$link ',' $msg ',' $type ',' $frnd ',' $lpub_time ')";
                      $r = mysqli_query($conn,$q);
                     // echo $q;
                      //var_dump($r);
                    if ($r) {
                       // echo 'values inserted';
                        $q1='SELECT * FROM submit_fb_posts ORDER BY local_post_id DESC LIMIT 1';
                        $r1 = mysqli_query($conn,$q1);
                        $row=  mysqli_fetch_row($r1);
                       $local_id=$row[0];                      
                    } else {                      
                        echo 'Sorry, an error occurred please try again...';
                    }
 $pname=$_POST[$grp];
                    $q3 = "INSERT INTO posts_details(user_id,local_post_id,publish_date,feed_place,feed_place_id,post_type,place_name) VALUES('$user','$local_id','$lpub_time ','$feed_place','$frnd','$type','$pname')";                  
                    $r3 = mysqli_query($conn,$q3);
                    if (!$r3) {                       
                        echo 'Sorry, an error occurred please try again...';
                    }
                      echo '<div class="alert alert-success">Your post '.($i+1).' will be posted at '.$lpub_time.'</div>'; 
                }
                $i++;
                $tno++;
            }
        }
    }
 $fb_post_id = array();
    $delete_post = array();
    $query = "SELECT * FROM `posts_details` WHERE `user_id` = '$user'";
    $result = mysqli_query($conn,$query);
    if($result){
        while($row = mysqli_fetch_array($result)){
            $fb_post_id[] = $row['fb_post_id'];
        }
    }
    foreach ($fb_post_id as $key){
        if($key != ''){
      try {
        $user_profile = $facebook->api('/'.$key ,'GET');
        
      } catch(FacebookApiException $ex) {
          $delete_post[] = $key;
      }
    } 
    }

    if($delete_post){
        foreach ($delete_post as $key){
            $update = "UPDATE `posts_details` SET `deleted_post`= 1 WHERE `fb_post_id` = '$key' " ;

             mysqli_query($conn, $update);
        }
    }
   $q = "SELECT * FROM submit_fb_posts WHERE user_id='$user'";
$r = mysqli_query($conn, $q);
if($r){
while ($row = mysqli_fetch_assoc($r)) {   
    $tempt = new DateTime($row['publish_time']);
    $time = $tempt->format('U');
    $now1 = new DateTime('now');
    $now = $now1->format('U');
    //var_dump($now);
    //var_dump($time);
    if ($time <= $now) {
       // echo 'next';
        $u_id = trim($row['user_id']);
        $link = trim($row['link']);
        $msg = trim($row['msg']);
        $feed_id = trim($row['feed_place_id']);
        $local_id = trim($row['local_post_id']);
        $q1 = "SELECT * FROM user_fb_profile where user_fb_id=$u_id";       
        $r1 = mysqli_query($conn, $q1);       
        $row1 = mysqli_fetch_row($r1);        
        $token = trim($row1[4]);       
        $ptype=trim($row['post_type']);
        if ($ptype == 'write_post') {
            try{
            $ret_obj = $facebook->api('/' .$feed_id. '/feed', 'POST', array(
                'link' => $link,
                'message' => $msg,
                'access_token' => $token,
            ));            
           // echo '<pre>Posted with:Post ID: ' . $ret_obj['id'] . '</pre>';
            } catch (Exception $ex) {
              // echo 'your post cannot be posted'.$ex;
            }
            }        
        if ($ptype == 'giving_url') {
            try{
            $ret_obj = $facebook->api('/' . $feed_id . '/photos', 'POST', array(
                'url' => $photo,
                'message' => $message,
                'access_token' => $token,
                    )
            );
          // echo '<pre>Posted with:Post ID: ' . $ret_obj['id'] . '</pre>';
            
            } catch (Exception $ex) {
              //  echo 'your post cannot be posted'.$ex;
            }
           
            }
        if ($ptype == 'via_upload') {
            $type = pathinfo($link, PATHINFO_EXTENSION);
           //$c= 'temp_img';
          // var_dump($c);
            $photo = $_SERVER["DOCUMENT_ROOT"]."/temp_img/".$link;
           // return FALSE;
            try{
            $ret_obj = $facebook->api('/' . $feed_id . '/photos', 'POST', array(
                'source' => "@$photo",
                'message' => $msg,
                'access_token' => $token,
                    )
            );
          // echo '<pre>Posted with:Post ID: ' . $ret_obj['id'] . '</pre>';  
            } catch (Exception $ex) {
              //  echo 'your post cannot be posted'.$ex;
            }
        }
        if (isset($ret_obj)) {
            $fb_id = $ret_obj['id'];
        }
        $q3 = "UPDATE posts_details SET published='1', fb_post_id='$fb_id' WHERE local_post_id='$local_id'";
        $r3 = mysqli_query($conn, $q3);
        if ($r3) {
           // echo 'updated';
        } else {
            //echo 'error in update';            
        }
        $q4 = "DELETE FROM submit_fb_posts where local_post_id='$local_id'";
        $r4 = mysqli_query($conn, $q4);
        if ($r4) {
           // echo 'updated2';
        } else {
           // echo 'error in update2';
           // die($mysqli->error());
        }
           if ($ptype == 'via_upload') {
             $q5="SELECT * FROM submit_fb_posts where link='$link'";
             $r5=  mysqli_query($conn, $q5);
             $no=  mysqli_num_rows($r5);
             if($no<1){
             //echo 'upload';
             unlink($photo);
             $v=file_exists ( $photo);
             if($v){
                // echo 'yes';
             }else{
                 //echo 'no';
             }
             }
         }
    }
}     
}
}
} else {
    $login_url = $facebook->getLoginUrl(array('scope' => 'read_stream,email,user_friends,publish_stream,friends_online_presence,read_friendlists,user_groups,user_likes,publish_actions'));
    echo '<div class="alert alert-danger" role="alert"><span style="color:#990000;" class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>';
    echo ' Please login <a href="' . $login_url . '">here</a> first.</div>';
}