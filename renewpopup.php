<?php  include('php/connection.php');
$conn = mysqli_connect($host, $username, $password, $database)or die("connection lost"); ?>

<!doctype html>
<html>
    <head>
        <title>renew user duration</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="style.css">
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
        <link type="text/css" href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" /> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </head>
    <body>
        <div class="container">
            <form class="form-horizontal" role="form" method="post">
                <div class="form-group has-success has-feedback">
                                <h4>Select Any One Duration</h4>
                              <label class="control-label col-sm-2" for="start_date">Start Date</label>
                              <div class="col-sm-4">
                                  <input type="date" class="form-control" id="start_d" name="sdate" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon sdate form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="sdatereq"></span>
                              </div>
                             </div>
                            <div class="form-group has-success has-feedback">
                              <label class="control-label col-sm-2" for="end_date">End Date</label>
                              <div class="col-sm-4">
                                  <input type="date" class="form-control" id="end_d" name="edate" aria-describedby="inputSuccess3Status">
                                <span class="glyphicon edate form-control-feedback" aria-hidden="true"></span>
                                <span id="inputSuccess3Status" class="sr-only">(success)</span>
                                <span class="edatereq"></span>
                              </div>
                            </div>
                            <button class="btn btn-primary" id="regis" type="submit" name="field_date" >Renew Date</button>
            </form>
            <span>Or</span>
            <form class="form-horizontal" role="form" method="post">
                <label class="control-label col-sm-2" for="start_date">Days</label>
                <select name="days" id="day" class="form-control">
                    <option value="null" selected="selected">---</option>
                    <?php 
                    for($i=1;$i<=31;$i++):
                    ?>
                    <option value="<?php echo $i ?>"><?php echo $i ?>days</option>
                    <?php endfor; ?>
                </select>
                <label class="control-label col-sm-2" for="start_date">Months</label>
                <select name="months" id="month" class="form-control">
                    <option value="null" selected="selected">---</option>
                    <?php 
                    for($i=1;$i<=12;$i++):
                    ?>
                    <option value="<?php echo $i ?>"><?php echo $i ?>months</option>
                    <?php endfor; ?>
                </select>
                <label class="control-label col-sm-2" for="start_date">Years</label>
                <select name="years" id="year" class="form-control">
                    <option value="null" selected="selected">---</option>
                    <?php 
                    for($i=1;$i<=20;$i++):
                    ?>
                    
                    <option value="<?php echo $i ?>"><?php echo $i ?>years</option>
                    <?php endfor; ?>
                </select>
                <span class="date"></span>
                <button class="btn btn-primary" id="registration1" type="submit" name="drop_date" >Renew Date</button>
            </form>
            <?php
              if(isset($_POST['field_date'])){
                $id = $_GET['i'];
                $sdate = $_POST['sdate'];
                $edate = $_POST['edate'];
                $update = "UPDATE `user_registration` SET `start_date`='".$sdate."',`end_date`='".$edate."' WHERE `user_id` = '".$id."' ";
                $ok = mysqli_query($conn, $update);
                
            }
            if(isset($_POST['drop_date'])){
                $id = $_GET['i'];
                $day = $_POST['days'];
                $month = $_POST['months'];
                $year = $_POST['years'];
                $sdate = date('Y-m-d');
                if($month){
                    $day += $month*30;
                }
                if($year){
                    $day += $year*365;
                }
                //var_dump($sdate->date);
                $td= strtotime("+".($day).'day');
                $edate = date("Y-m-d",$td);
                //echo $sdate."--".$edate;
                $update = "UPDATE `user_registration` SET `start_date`='".$sdate."',`end_date`='".$edate."' WHERE `user_id` = '".$id."' ";
                $ok = mysqli_query($conn, $update);
            }
           
            ?>
        </div> 
        <script>
       $(document).ready(function(){
                $('#regis').click(function(e){
                     var sdate = $('#start_d').val();
     if( sdate == 0){
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Start Date Required');
            e.preventDefault();
        } else{
            if(isDate(sdate)){
            $('span.sdate').addClass('glyphicon-ok');
            
        }else{
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Invalid Date Format Please Enter Date in YYYY-MM-DD');
            e.preventDefault();
        }
        }
     var edate = $('#end_d').val();
     if( edate == 0){
         $('span.edate').addClass('glyphicon-remove');
            $('span.edatereq').html('End Date Required');
            e.preventDefault();
        } else{
            if(isDate(edate)){
            $('span.sdate').addClass('glyphicon-ok');
        }else{
            $('span.sdate').addClass('glyphicon-remove');
            $('span.sdatereq').html('Invalid Date Format Please Enter Date in YYYY-MM-DD');
            e.preventDefault();
        }
        }   
                });
                
            $("button#registration1").click(function(e){
                
                var year = $("#year").val();
                var month = $("#month").val();
                var day = $("#day").val();
            if(year=='null' && month=='null' && day=='null'){
                $('span.date').html('min one field is required');
                e.preventDefault();
            }
            });
            var ok = <?php echo $ok; ?>;
            if(ok){
                window.close()}
            });
            </script>
    </body>
</html>